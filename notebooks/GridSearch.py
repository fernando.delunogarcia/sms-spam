from abc import ABC, abstractmethod
import AbstractClassifier as ac
import PerformanceMeasures as pm

class GridSearchModels(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def GetModel(self,i):
        pass

    @abstractmethod
    def Count(self):
        pass


class GridSearch(ac.AbstractClassier):
    def __init__(self, gs_models):
        self.models = []

        self.M = gs_models.Count()

        #cria um modelo para cada combinação de parâmetros estabelecida
        for i in range(self.M):
            self.models.append(gs_models.GetModel(i))

    def GetModelName(self):
        return 'GridSearch'


    def Train(self, X, Y):
        #treina todos os modelos
        for j in range(self.M):
            model = self.models[j]

            if model is not None:
                print('Training {}'.format(model.GetModelName()))
                model.Train(X, Y)

    def Predict(self, X, Y=None):
        fm = pm.PerformanceMeasures()
        reports = []
        
        #faz a validação de cada modelo e retorna os relatórios parciais
        for j in range(self.M):
            model = self.models[j]

            if model is not None:
                print('Validation Testing {}'.format(model.GetModelName()))
                Y_pred = model.Predict(X)
                report = fm.EvaluatePerformanceMetrics(Y, Y_pred)
                print(report)
                reports.append(report)
        return reports

    def SaveModel(self, path):
        pass

    def Count(self):
        return self.M

    def GetModel(self,i):
        if (i >= 0) and (i < self.M):
            return self.models[i]
        return None