import numpy as np
import CrossValidation as cv
import KNN

class KNN_Models():
    def __init__(self):
        self.Knn = [1,3,5]

    def GetModel(self,i):
        if (i < 0) or (i >= 3*len(self.Knn)):
            return None

        dist = int(i/3)
        k = i%3
        

        return KNN.KNearestNeighbors(self.Knn[k],dist)

    def Count(self):
        return 3*len(self.Knn)
        


def GetKNN(K,model,token):
    #mudar o bow para o tipo desejado
    bow = 'BIN'
    search = cv.CrossValidation('KNN', K, model, token, bow, KNN_Models())
    return search
