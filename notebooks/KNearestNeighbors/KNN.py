from pathlib import Path
import sys
d = Path().resolve().parent
sys.path.append('../')

import AbstractClassifier as ac
import numpy as np

class KNearestNeighbors(ac.AbstractClassier):
    def __init__(self, K,dist):
        self.K = K
        self.dist = dist

    def GetModelName(self):
        DistType = ['Euclidiana', 'cos similarity', 'Hamming']

        return 'KNN ' + 'K=' + str(self.K) + ' Dist: ' + DistType[self.dist]

    def Train(self, X, Y):
        self.Y = np.array(Y, dtype=int)
        self.Xsamples = X

    def Predict(self, X):
        Ypred = np.zeros(len(X))
        
        for i in range(len(X)):
            Ypred[i] = self.__FindNearestNeighbor(X[i])
        return Ypred

    def SaveModel(self, path):
        pass

    def __FindNearestNeighbor(self,x):
        y = 0
        ind_viz = np.ones(self.K, dtype=int)
        D = self.distance(x, self.Xsamples)
        ind_viz = np.argsort(D)[0:self.K]
        y = np.bincount(self.Y[ind_viz]).argmax()
        return y

        
    def distance(self, x, X):
        m = X.shape[0] # Quantidade de objetos em X
        D = np.zeros(m) # Inicializa a matriz de distâncias D
        for i in range(m):
            if(self.dist == 0):
                #euclidiana
                D[i] = np.sqrt(np.sum((x-X[i])**2))
            elif(self.dist == 1):
                #cos similarity
                D[i] = np.dot(x, X[i])/(np.linalg.norm(x)*np.linalg.norm(X[i]))
            elif(self.dist == 2):
                #Hamming
                D[i] = np.count_nonzero(x!=X[i])
        
        return D