import numpy as np
import pandas as pd


class DatasetStatistics:

    def __init__(self, BagOfWords):
        self.df_BagOfWords = BagOfWords
        

    def __Entropy(self,pp,pn):
        e = 0
        if pp != 0:
            e += -pp*np.log2(pp)
        if pn != 0:
            e += -pn*np.log2(pn)
            
        return e

    def EvaluateInformationGain(self):
        # exibe a quantidade de SMS por classe
        attr_class = self.df_BagOfWords.columns.values[self.df_BagOfWords.shape[1]-1]
        #print(attr_class)
        desc = self.df_BagOfWords.groupby(attr_class).count()

        self.hams = np.sum(self.df_BagOfWords[attr_class] == 0)
        self.spams = np.sum(self.df_BagOfWords[attr_class] == 1)
        self.total = self.hams+self.spams
        self.pham = self.hams/self.total
        self.pspam = self.spams/self.total
        self.entropy = self.__Entropy(self.pham,self.pspam)
 
        print('Calculando entropia por atributo...')

        self.IG = np.zeros(self.df_BagOfWords.shape[1])
        
        
        k = 0
        values_by_class = self.df_BagOfWords.groupby(attr_class)
        
        for column in self.df_BagOfWords.columns.values[0:-1]:
            values = values_by_class[column].value_counts().index.tolist()
            ocurrences = values_by_class[column].value_counts()

            f0_ham = 0
            f0_spam = 0
            f1_ham = 0
            f1_spam = 0
            
            for index in values:
                if(index[1] == 0): #feature == 0
                    if(index[0] == 0): 
                        f0_ham = ocurrences[0][0]
                    else:
                        f0_spam = ocurrences[1][0]           
                else: #feature == 1      
                    if(index[0] == 0):
                        print(ocurrences)
                        f1_ham = ocurrences[0][1]
                    else:
                        f1_spam = ocurrences[1][1]
            
            f0_count = f0_ham + f0_spam
            f1_count = f1_ham + f1_spam
            
            entropy_f0 = self.__Entropy(f0_ham/f0_count, f0_spam/f0_count)
            entropy_f1 = self.__Entropy(f1_ham/f1_count, f1_spam/f1_count)
            
            self.IG[k] = self.entropy - ((f0_count/self.total)*entropy_f0) - ((f1_count/self.total)*entropy_f1)
            k+=1
        
        return self.IG


    def GetHighestInformationGain(self,N):
        IG_idx = None

        if (N>0):
            IG_idx = np.asarray([np.argsort(self.IG)[::-1][:N]])

        return IG_idx

    def GetInformationGainGreaterThan(self, alpha):
        IG_idx = None

        if (alpha>0):
            IG_idx = np.where(self.IG >= alpha)

        return IG_idx

    def GetMostFrequentTokens(self, N):

        ham_N = None
        spam_N = None

        if N > 0:
            attr_class = self.df_BagOfWords.columns.values[self.df_BagOfWords.shape[1]-1]

            grouped = self.df_BagOfWords.groupby(attr_class).sum()

            ham = grouped.sort_values(by=0, ascending=False, axis=1)
            spam = grouped.sort_values(by=1, ascending=False, axis=1)
            
            ham_N = ham.iloc[0,0:N]
            spam_N = spam.iloc[1,0:N]
    
        return ham_N, spam_N

"""
# umidade Vento Alvo
# 0,1      0,1   0,1
BAG = np.array([
    [0,0,0],
    [0,1,0],
    [0,0,1],
    [0,0,1],
    [1,0,1],
    [1,1,0],
    [1,0,1],
    [0,0,0],
    [1,0,1],
    [1,1,1],
    [1,1,1],
    [0,1,1],
    [1,0,1],
    [0,1,0]
] )

df_bag = pd.DataFrame(data=BAG, columns = ['Humidade', 'Vento', 'Alvo'])
print(df_bag)
st = DatasetStatistics(df_bag)
IG = st.EvaluateInformationGain()
print(IG)
#umidade 0.1518355  vento 0.04812703
"""
