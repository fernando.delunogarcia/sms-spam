import numpy as np
from abc import ABC, abstractmethod
import TokenUtils as tkutils

class ScoreMethod(ABC):
    def __init__(self):
        super().__init__()
        
    @abstractmethod
    def Transform(self,output,tokens,features):
        pass

class BinaryScore(ScoreMethod):
    def __init__(self):
        pass

    def Transform(self,output,tokens,features):
        
        for word in tokens:
            if word in features:
                idx = features[word]
                output[idx] = 1

        return output

class CountScore(ScoreMethod):
    def __init__(self):
        pass

    def Transform(self,output,tokens,features):
        for word in tokens:
            if word in features:
                idx = features[word]
                output[idx] += 1

        return output

class DocFrequencyScore(ScoreMethod):
    def __init__(self):
        pass

    def Transform(self,output,tokens,features):
        for word in tokens:
            if word in features:
                idx = features[word]
                output[idx] += 1
        
        #quantidade de termos no documento
        total = np.sum(output)

        if total > 0:
            #Normalização L2
            output = output / np.sqrt(np.sum(np.power(output,2)))

        return output

class InverseTermFrequency(ScoreMethod):
    def __init__(self, total, documents, smooth, tflog, idf=None):
        self.tflog = tflog

        if idf is None:
            #documents é uma bag of word binária
            #a soma das colunas é a ocrrência do termo nos documentos
            documents_count = np.sum(documents,axis=0)

            self.IDF = np.zeros(documents_count.shape)

            if smooth is False:
                nonzero = documents_count > 0  
                self.IDF[nonzero] = np.log((total)/(documents_count[nonzero]))
            else:
                self.IDF = np.log((2 + total)/(1 + documents_count))
        else:
            self.IDF = idf

        
    def Transform(self,output,tokens,features):
        
        for word in tokens:
            if word in features:
                idx = features[word]
                output[idx] += 1
        
        if self.tflog is True:
            output = np.log(1+output)

        
        #normalização L2
        den = np.sqrt(np.sum((output**2) * (self.IDF**2)))

        if(den > 0):   
            output =  (output*self.IDF)/den
        else:
            output = 0

        #print(np.sum(np.power(output,2)))

        return output