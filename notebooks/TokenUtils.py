# -*- coding: utf-8 -*-
import re
from abc import ABC, abstractmethod
import TextExtensionWebdriver as te
import csv
import string

import nltk
from textblob import TextBlob
from textblob import Word
from nltk.corpus import wordnet
from nltk.stem.wordnet import WordNetLemmatizer
import TextExtensionWebdriver as te
from nltk import pos_tag, word_tokenize
import pandas as pd

stop_words = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]

class Tokenizer(ABC):
    def __init__(self):
        super().__init__()
        self.RemoveStopWords = False
        self.RemovePunctuation = False
        self.ConvertNumbers = False

        self.localVocabulary = {}

    def SetupStopWordsRemoval(self,state):
        self.RemoveStopWords = state

    def SetupPunctuationRemoval(self,state):
        self.RemovePunctuation = state

    def SetupNumberConversion(self,state):
        self.ConvertNumbers = state

    @abstractmethod
    def TextTokenization(self,text):
        pass

    @abstractmethod
    def GetTokenizerName(self):
        pass

    def WriteTokens(self):
        try:
            with open("../dataset/dictionary.txt", 'w') as csvfile:
                for data in self.localVocabulary.keys():
                    row = str(data) + '\t' + str(self.localVocabulary[data]) + '\n'
                    csvfile.write(row)
        except IOError:
            print("I/O error")

    def LoadTokens(self,path):
        print('Carregando Dicionário')
        try:
            df = pd.read_csv(path, sep='\t', header=None, names = ["key", "value"])
            df["key"].dropna(axis=0,inplace=True)
            df.fillna('',inplace=True)

            self.localVocabulary = {}
            for i in range(df.shape[0]):
                self.localVocabulary[df["key"][i]] = df["value"][i]
            return df

        except IOError:
            print("I/O error")
        return None

class RegexTokenizer(Tokenizer):
    def __init__(self, rtype,regex):
        super(RegexTokenizer, self).__init__()
        self.Regex = regex
        self.RTK = rtype
        self.pregex = re.compile('[%s]' % re.escape(string.punctuation))

    def GetTokenizerName(self):
        return 'TOK_' + self.RTK

    def TextTokenization(self,text):
        #lista de tokens que será retornada
        tokens = []
    
        #todo texto será tratado como escrito em letras minúsculas
        text = text.lower()

        if self.RemovePunctuation is True:
            text = self.pregex.sub('', text)

        #usa o regex escolhido para processar a sequência
        token_str = re.findall(self.Regex,text,re.ASCII)
               
        if self.RemoveStopWords is True:
            token_str = [word for word in token_str if not word in stop_words]
        
        #inclui os tokens identificados pelo regex
        for i in range(len(token_str)):
            txt = token_str[i] 
            if (len(txt) > 1):
                corrected = ""

                if txt.isalpha():
                    corrected = txt
                else:
                    if txt.isalnum():
                        if self.ConvertNumbers is True:
                            corrected = re.sub(r"[0-9]", "N", txt)

                if(corrected != ""):
                    tokens.append(corrected)
                
        return tokens


class RegexPipelineTokenizer(Tokenizer):
    def __init__(self, rtype, regex,use_dict=False):
        super(RegexPipelineTokenizer, self).__init__()

        self.UseDict = use_dict
        self.RTK = rtype
        self.Regex = regex

        #regex para processamento da string antes da tokenização
        self.RegexEmail = re.compile(r'\b[\w\-.]+?@\w+?\.\w{2,4}\b')
        self.RegexUrl = re.compile(r'(http[s]?\S+)|(\w+\.[A-Za-z]{2,4}\S*)')
        self.RegexEmoji = re.compile(r'=\[|=\]|=\)|:\[|:\]|:\)|:-\)|:\(|:-\(|;\);-\)|:-O|8-|:P|:D|:\||:S|:\$|:@|8o\||\+o\(|\(H\)|\(C\)|\(\?\)')

        self.RegexDates = re.compile(r'((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$')
        self.RegexCurrencySymbols = re.compile(r'£|\$')
        self.RegexPhoneNumber = re.compile(r'\b(\+\d{1,2}\s)?\d?[\-(.]?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}\b')
        self.RegexNumbers = re.compile(r'\d+(\.\d+)?')
        #self.RegexPunctuation = re.compile(r'[^\w\d\s]+')
        self.RegexPunctuation1 = re.compile(r'[\']')
        self.RegexPunctuation2 = re.compile(r'[^\w\d\s]+|[\!\"\#\$\%\&\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\\\]\^_\`\{\|\}\~]')
        self.RegexNoncharacter = re.compile(r'[\s+|\s+?$]')
        self.lem = WordNetLemmatizer()

        self.RegexPipeline = [
            self.RegexEmail,
            self.RegexUrl,
            self.RegexEmoji,
            self.RegexDates,
            self.RegexCurrencySymbols,
            self.RegexPhoneNumber,
            self.RegexNumbers,
            self.RegexPunctuation1,
            self.RegexPunctuation2,
            self.RegexNoncharacter
        ]
        
        self.RegexOutputs = [
            ' EMAIL ',
            ' URL ',
            ' EMOJI ',
            ' DATE ',
            ' CURRENCY ',
            ' PHONE ',
            ' NUMBER ',
            '',
            ' ',
            ' '
        ]

        self.wordnet_words = dict.fromkeys(nltk.corpus.words.words(), None)


    def GetTokenizerName(self):
        return 'TOK_' + self.RTK

    def __is_valid_word(self,word):
        try:
            x = self.wordnet_words[word]
            print('english word:', word)
            return True
        except KeyError:
            return False

    def TextTokenization(self,text):
        #lista de tokens que será retornada
        tokens = []
    
        #todo texto será tratado como escrito em letras minúsculas
        raw = text.lower()

        #pipeline de conversão do texto
        txt = raw
        #print(txt)
        i = 0
        for regex in self.RegexPipeline:
            txt = regex.sub(self.RegexOutputs[i], txt)
            i += 1

        #usa o regex escolhido para processar a sequência
        token_str = re.findall(self.Regex,txt,re.ASCII)
       

        if self.RemoveStopWords is True:
            token_str = [word for word in token_str if not word in stop_words]

        #print(token_str)
        for i in range(len(token_str)):

            txt = token_str[i] 
            
            if (len(txt) > 1):
                if(txt in self.localVocabulary.keys()):
                    final_tk = self.localVocabulary[txt]
                else:
                    if self.UseDict is True: #adicionado para não necessitar de acesso ao TextExpansion
                        final_tk = ""
                    else:
                        if txt.islower():
                            final_tk = ""

                            web_tk = te.WebDriverUtils.FindToken(txt)
                            while(web_tk is None):
                                web_tk = te.WebDriverUtils.FindToken(txt)

                            if web_tk == txt:

                                corrected = TextBlob(txt).words[0].spellcheck()
                                        
                                if(corrected[0][1] == 1):
                                    if(corrected[0][0] in self.localVocabulary):
                                        final_tk = self.localVocabulary[corrected[0][0]]
                                    else:
                                        txt = corrected[0][0]

                                        if self.__is_valid_word(txt): #verifica se a palavra existe no vocabulário
                                            word_tag = pos_tag(word_tokenize(txt))
                                            wntag = word_tag[0][1].lower()
                                            wntag = wntag[0] if wntag[0] in ['a', 'r', 'n', 'v'] else None
                                            final_tk = self.lem.lemmatize(txt, wntag) if wntag else txt 
                            else:
                                if web_tk.isdigit():
                                    web_tk = "ORDINAL"

                                print('texpansion', txt, web_tk)
                                final_tk = web_tk  
                        else:
                            final_tk = txt
                #adiciona a palavra real no dicionário mas usa como token somente as validadas anteriormente
                

                if final_tk != "":
                    self.localVocabulary[txt] = final_tk
                    tokens.append(final_tk)
                else:
                    self.localVocabulary[txt] = final_tk
        #print(self.localVocabulary)
        return tokens

RegexTokenizers = {
    'P': RegexTokenizer('P',"[^\s]+"),
    'C': RegexTokenizer('C',"[^\s][-.,:\w]*[^\s]?"),
    'S': RegexTokenizer('S',"[^\s][-\w]*[^\s]?"),
    'X': RegexTokenizer('X',"[^\s][\/!?#]?[-\w]*(?:[\"’=;]|\/?>|:\/*)?"),
    'F': RegexTokenizer('F',"[^\s][-.,;:\/@?!'[[-[A-Za-z0-9]+|[A-Za-z]]?|[?:\.|,|\d*]+]*]*[^\s]?"),
    'PF': RegexPipelineTokenizer('PF',"[^\s]+")
}

def getTokenizers():
    return RegexTokenizers.keys()

def getTokenizer(tok):
    rtok = RegexTokenizers.get(tok)
    if rtok is None:
        print("Tokenizador informado não existe.")
        rtok = RegexTokenizers.get('P')
    
    return rtok


def TestTokenizer(type):
    tk = getTokenizer(type)

    txt = "i am i'm it he's going to email cooked bed now prin GSOH? Good with SPAM the ladies?U could b a male gigolo? 2 join the uk's fastest growing mens club reply ONCALL. mjzgroup. 08714342399.2stop reply STOP. msg@£1.50rcvd"
    output = tk.TextTokenization(txt)
    print(output)


    #tk.WriteTokens()

#TestTokenizer('PF')