import numpy as np

def stratified_holdOut(target, pTrain):
    """
    Retorna os indices dos dados de treinamento e teste 
    
    Parâmetros
    ----------   
    target: vetor com as classes dos dados
    
    pTrain: porcentagem de dados de treinamento
    
    Retorno
    -------
    train_index: índices dos dados de treinamento 
    test_index: índices dos dados de teste 
    """
    folds_final = np.zeros(1,dtype='object')

        

    # inicializa as variaveis que precisam ser retornadas 
    train_index = []
    test_index = []
    

    classes = np.unique(target)
    
    for j in enumerate(classes):
        #seleciono o índices para classe Cj
        train_set_class = np.where(target==j[1])
        #obtenho a quantidade de elementos para pTrain% de Cj 
        max_class = pTrain * train_set_class[0].shape[0]
        #seleciono os primeiros pTrain% dos vetor de Cj. O restante é armazenado no teste
        train_index = np.concatenate([train_index, train_set_class[0][:np.int(max_class)]]).astype(int)
        test_index = np.concatenate([test_index, train_set_class[0][np.int(max_class):]]).astype(int)
    #ordeno os índices selecionados para teste e treino
    train_index = np.sort(train_index)
    test_index = np.sort(test_index)
    
    ##################################################################################
    folds_final[0] = np.array( [train_index,test_index] )
    
    return folds_final