from abc import ABC, abstractmethod

class AbstractClassier(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def GetModelName(self):
        pass

    @abstractmethod
    def Train(self,X,Y):
        pass

    @abstractmethod
    def Predict(self,X,Y=None):
        pass

    @abstractmethod
    def SaveModel(self, path):
        pass