import LogisticRegression as logreg
import numpy as np
import CrossValidation as cv

class LogisticRegression_Models():
    def __init__(self):
        self.reg = [0,0.5,1]

    def GetModel(self,i):
        if (i < 0) or (i >= len(self.reg)):
            return None

        return logreg.LogisticRegression(self.reg[i])

    def Count(self):
        return len(self.reg)


def GetLogisticRegression(K,model,token):
    bow = 'TFIDF'
    search = cv.CrossValidation('Logitic Regression', K, model, token, bow, LogisticRegression_Models())
    return search
