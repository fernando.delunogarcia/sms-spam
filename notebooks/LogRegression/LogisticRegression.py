from pathlib import Path
import sys
d = Path().resolve().parent
sys.path.append('../')

import AbstractClassifier as ac
import numpy as np
import scipy.optimize  


class LogisticRegression(ac.AbstractClassier):
    def __init__(self,alpha):
        self.alpha = alpha

        
    def GetModelName(self):
        return 'LogisticRegression reg %1.3f'%self.alpha

    def Train(self, X, Y):
        
        m, n = X.shape
        Xsamples = np.column_stack( (np.ones(m),X) ) # Adiciona uma coluna de 1s em x
        
        #iteracoes = 1500
        MaxIter = 1500
        theta = np.zeros(n+1)

        """
        result = scipy.optimize.minimize(fun=self.__funcaoCusto, x0=theta, args=(Xsamples, Y),  
                        method='BFGS', jac=True, options={'maxiter': MaxIter, 'disp':True})
        """

        result = scipy.optimize.minimize(fun=self.__funcaoCustoReg, x0=theta, args=(Xsamples, Y, self.alpha),  
                method='BFGS', jac=True, options={'maxiter': MaxIter, 'disp':True})

        self.theta = result.x


    def Predict(self, X):
        m = X.shape[0]
        p = np.zeros(m, dtype=int)

        if(X.shape[1]!=self.theta.shape[0]):
            X = np.column_stack( (np.ones(m),X) )
        
        p = self.__sigmoid(np.dot(X, self.theta)) 
        p[p>=0.5] = 1
        p[p<0.5] = 0
        return p

    def SaveModel(self, path):
        pass

    def __sigmoid(self, z):
        if isinstance(z, int):
            g = 0
        else:
            g = np.zeros(z.shape)
        g = 1/(1+np.exp(-z))
        return g
    
    def __funcaoCustoReg(self,theta, X, Y, lambda_reg):

        # Initializa algumas variaveis uteis
        m = len(Y) #numero de exemplos de treinamento

        # Voce precisa retornar a seguinte variavel corretamente
        J = 0
        grad = np.zeros( len(theta) )

        eps = 1e-15
        
        reg = lambda_reg/(2*m) * np.sum(theta[1:] ** 2)
        
        h = self.__sigmoid( np.dot(X,theta) )
        
        J = np.dot( -Y,np.log(h+eps) ) - np.dot( (1-Y),np.log(1-h+eps) )
        J = (1/m)*J + reg
        
        grad = np.dot( (h-Y),X ) 
        grad = (1/m) * grad
        
        # usa a regularização no gradiente
        grad[1:] = grad[1:] + (lambda_reg/m) * theta[1:]

        return J, grad
    """
    def __funcaoCusto(self,theta, X, Y):
        m = len(Y)
        J = 0
        grad = np.zeros( len(theta) )
        eps = 1e-15
        h = np.dot(X, theta)
        h_ = self.__sigmoid(h)
        J = (np.dot(np.log(eps+h_),-Y) - np.dot((1-Y),np.log(eps+1-h_)))/m
        grad = np.dot((h_-Y),X)/m    
        return J, grad
    """

