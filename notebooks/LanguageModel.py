from abc import ABC, abstractmethod

class LanguagemModel(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def Formalize(self,tokens):
        pass

    @abstractmethod
    def GetModelName(self):
        pass

class Ngram(LanguagemModel):
    def __init__(self, n):
        self.N = n

    def GetModelName(self):
        return str(self.N)+'-gram'
 
    def Formalize(self,tokens):
        window_size = self.N
        #novos tokens que serão gerados
        OSB_tokens = []
        
        #a função deve receber como parÂmetro uma janela 1 < window_size
        if (window_size < 2):
            print("A janela deve ser maior ou igual a dois.")
            is_finished = True
        else:
            is_finished = False
        
        #ponto de partida da janela
        i = 0
        
        while is_finished is False:
            
            # se a janela está no final do stream de tokens é necessário reduzi-la
            if (len(tokens) - i - window_size) >= 0:
                #seleciona os tokens da janela
                wd_tokens = tokens[i:i+window_size]

                #crio os novos tokens contendo a combinação de N palavras
                ngram = wd_tokens[0]
                for j in range(1,window_size):
                    ngram += ' ' + wd_tokens[j]
                    
                #ngram.join(wd_tokens[window_size-1])
                OSB_tokens.append(ngram)
                
                #desloca janela
                i += 1
            else:
                i = 0
                window_size = window_size - 1
                
                if (window_size == 0):
                    is_finished = True
        
        return OSB_tokens

class OrthogonalSparseBigram(LanguagemModel):
    def __init__(self, n):
        self.N = n

    def GetModelName(self):
        return ('OSB-'+str(self.N))
 
    def Formalize(self, tokens):
        window_size = self.N

        #novos tokens que serão gerados
        OSB_tokens = []
        
        #a função deve receber como parÂmetro uma janela 1 < window_size < 11
        if (window_size < 2) or (window_size > 10):
            print("A janela deve ser maior ou igual a dois e menor ou igual a 10.")
            is_finished = True
        else:
            is_finished = False
        
        if (len(tokens) < 2):
            is_finished = True
            
        if (len(tokens) < window_size):
            window_size = len(tokens)
            
        #ponto de partida da janela
        i = 0

        while is_finished is False:
            
            # se a janela está no final do stream de tokens é necessário reduzi-la
            if (len(tokens) - i - window_size) < 0:
                window_size = window_size - 1
                
            #seleciona os tokens da janela
            wd_tokens = tokens[i:i+window_size]
            skip = '_'

            #print(window_size, wd_tokens)
            #crio os novos tokens contendo a palavra de início e a distância para as demais
            for j in range(1,window_size):
                OSB_tokens.append(wd_tokens[0] + skip + wd_tokens[j])
                skip += '_'

            #desloca janela
            i += 1
            
            ##verifica se a operação atual envolveu os dois ultimos elementos do stream
            if((len(tokens) - i) < 2):
                is_finished = True
        
        return OSB_tokens

Models = {
    '2-gram': Ngram(2),
    '3-gram': Ngram(3),
    '4-gram': Ngram(4),
    '5-gram': Ngram(5),
    'OSB-3' : OrthogonalSparseBigram(3),
    'OSB-4' : OrthogonalSparseBigram(4),
    'Normal': None
}

def getLanguageModels():
    return Models.keys()

def getLanguageModel(model):
    tok_model = Models.get(model)
    if (model != 'Normal') and (tok_model is None):
        print("Modelo informado não existe.")
        tok_model = None
    
    return tok_model