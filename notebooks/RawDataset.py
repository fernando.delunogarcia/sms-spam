import numpy as np
import pandas as pd



class RawDataset():
    def __init__(self, tokenizer, model, path, name):
        self.Tokenizer = tokenizer
        self.Model = model
        self.Path = path
        self.Name = name

    def GetDatasetName(self):
        if self.Model is None:
            return self.Name + '_' + 'Normal' + '_' + self.Tokenizer.GetTokenizerName()
        else:
            return self.Name + '_' + self.Model.GetModelName() + '_' + self.Tokenizer.GetTokenizerName()

    def Load(self,new_feature_vector):

        self.df_dataset = pd.read_csv(self.Path, sep='\t', header=None, names = ["Classe", "SMS"])
        self.hams = np.sum(self.df_dataset["Classe"] == 0)
        self.spams = np.sum(self.df_dataset["Classe"] == 1)
        self.total = self.hams+self.spams
        self.pham = self.hams/self.total
        self.pspam = self.spams/self.total

        print('\nDataset carregado: ' + self.Name)
        print('Quantidade de SMS: ', self.total)
        print('SMS Ham: ', self.hams)
        print('SMS Spam: ', self.spams)
        print('Probabilidade SMS Ham: ', self.pham)
        print('Probabilidade SMS Spam: ', self.pspam)

        if new_feature_vector is None:
            return self.__CreateVocabulary()
        else:
            self.__SetNewFeatures(new_feature_vector)
            return None
        

    def Show(self, n):
        # mostra as primeiras amostras do dataset
        print(self.df_dataset.head(n=n))

    def __SetNewFeatures(self,new_feature_vector):
        self.feature_vector = new_feature_vector
        self.vocabulary = {}
        k_features = 0
        for feature in new_feature_vector:
            if(feature is not "sms_class"):
                self.vocabulary.update({feature : k_features})
                k_features+=1

    def __CreateVocabulary(self):
        self.vocabulary = {}
        k_features = 0
        i = 0
        #cria o vocabulário com base em todos os documentos passados para função
        for text in self.df_dataset["SMS"]:
            text = str(text)
            i+=1
            #tk = self.Tokenizer.TextTokenization(text)
            tk = text.split(',')
            
            #Se indicado, usa a função para transformação de dados
            if(self.Model is not None):
                tk = self.Model.Formalize(tk)
            
            #para cada token do documento, faço a inclusão se não existir ou atualizo o valor de contagem
            for word in tk:
                    entry = self.vocabulary.get(word)
                    if entry is None:
                        self.vocabulary.update({word : k_features})
                        k_features+=1

        self.feature_vector = []
        for key, value in self.vocabulary.items():
            self.feature_vector.append(key)
        self.feature_vector.append("sms_class")

        return self.vocabulary, self.feature_vector


    def CharacterizeDocuments(self, score_method, new_feature_vector=None):

        BagOfWords = np.zeros((self.df_dataset.shape[0],len(self.feature_vector)))
        print(BagOfWords.shape)
        if score_method is not None:   
            #percorre o dataset de mensagens para extrair os tokens e preencher o vetor de atributos de cada amostra
            for i in range(self.df_dataset.shape[0]):
                text = self.df_dataset["SMS"].iloc[i]
                smsclass = self.df_dataset["Classe"].iloc[i]
                
                #tk = self.Tokenizer.TextTokenization(text)
                #text = text.replace(".", ",")
                #print(text)
                tk = text.split(',')
                
                
                if(self.Model is not None):
                    tk = self.Model.Formalize(tk)
            
                BagOfWords[i,:-1] = score_method.Transform(BagOfWords[i,:-1], tk, self.vocabulary)
                BagOfWords[i,BagOfWords.shape[1]-1] = smsclass

        return BagOfWords