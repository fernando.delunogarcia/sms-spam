import AbstractClassifier as ac
import numpy as np
import pandas as pd
import scipy
import scipy.optimize

class MultilayerPerceptron(ac.AbstractClassier):
    # Parametriza a rede -> POSTERIORMENTE IREI TESTAR A ADIÇÃO DE MAIS LAYERS
    def __init__(self, num_hidden_1, num_hidden_2, num_classes, lambda_value, nmax_i):
        '''
        Inicializa o objeto parametrizando a rede neural, de acordo com os parâmetros recebidos,
        criando os layers de entrada, hidden e de saída. Função também carrega os pesos das redes (Theta)

        Recebe como parâmetro:

        num_input - tamanho da camada de entrada
        num_hidden - tamanho da camada oculta
        num_classes - numero de classes possiveis
        theta_pth - diretório para os arquivos de peso
        '''
        self.hidden_layer_1_size = num_hidden_1   # Número de neuronios da camada intermediária
        self.hidden_layer_2_size = num_hidden_2
        self.num_labels = num_classes          # Número de classes do problema
        
        self.lambda_value = lambda_value 
        self.nmax_i = nmax_i
    
    def GetModelName(self):
        return self.__class__.__name__
    
    # Função que gera pesos iniciais
    def gen_random_weights(self, size_in, size_out, random_value = None):
        '''
        size_in = Quantidade de entradas do layer
        size_out = Quantidade de saídas do layer
        random_value = Define um valor random "Padrão"
        '''
        epsilon_init = 0.12
        
        if random_value is not None:
            W = np.random.RandomState(random_value).rand(size_out, 1 + size_in) * 2 * epsilon_init - epsilon_init
        else:
            W = np.random.rand(size_in, 1 + size_out) * 2 * epsilon_init - epsilon_init
            
        return W

    # Função de ativação sigmoidal
    def initial_weights(self):
        initial_Theta1 = self.gen_random_weights(self.input_layer_size, self.hidden_layer_1_size, random_value = 10)
        initial_Theta2 = self.gen_random_weights(self.hidden_layer_1_size, self.hidden_layer_2_size, random_value = 20)
        initial_Theta3 = self.gen_random_weights(self.hidden_layer_2_size, self.num_labels, random_value = 30)

        self.initial_rna_params = np.concatenate([np.ravel(initial_Theta1), np.ravel(initial_Theta2), np.ravel(initial_Theta3)])
        # print('Tamanho dos parametros de teta: ', len(self.initial_rna_params))
        
    # @cuda.jit
    def sigmoid(self, z):
        # start = timer()
        z = 1 / (1 + np.exp(-z))
        # duration = timer() - start
        # print('[INFO] Tempo de processamento do sigmoid: {}'.format(duration))
        return z
    
    # Gradiente da função de ativação sigmoidal
    #@vectorize([])
    def sigmoid_gradient(self, z):
        # start = timer()
        g = np.zeros(z.shape)
        g = 1 / (1 + np.exp(-z))
        g = g * (1 - g)
        # duration = timer() - start
        # print('\n[INFO] Tempo de processamento do sigmoid_gradiente: {}'.format(duration))
        return g

    def funcaoCusto_backp_reg(self, new_nn, X, y, vLambda):
        '''
        Implementa a funcao de custo para a rede neural com três camadas
        voltada para tarefa de classificacao
        
        Calcula o custo e gradiente da rede neural. 
        Os parametros da rede neural sao colocados no vetor nn_params
        e precisam ser transformados de volta nas matrizes de peso.
        
        input_layer_size - tamanho da camada de entrada
        hidden_layer_size - tamanho da camada oculta
        num_labels - numero de classes possiveis
        lambda - parametro de regularizacao
        
        O vetor grad de retorno contem todas as derivadas parciais
        da rede neural.
        '''
        #start = timer()
        
        eps = 1e-15

        if len(new_nn) > 0:
            self.nn_params = new_nn

        # Extrai os parametros de nn_params e alimenta as variaveis Theta1 e Theta2.

        # print('### Exibe os formatos dos Thetas ###')
        Theta1 = np.reshape( self.nn_params[0:self.hidden_layer_1_size*(self.input_layer_size + 1)], (self.hidden_layer_1_size, self.input_layer_size + 1) )
        # print('Formato Theta 1', Theta1.shape)
        Theta2 = np.reshape( self.nn_params[self.hidden_layer_1_size*(self.input_layer_size + 1):(self.hidden_layer_1_size*(self.input_layer_size + 1) + self.hidden_layer_2_size*(self.hidden_layer_1_size + 1))], 
                            (self.hidden_layer_2_size, self.hidden_layer_1_size+1) )
        # print('Formato Theta 2', Theta2.shape)
        Theta3 = np.reshape( self.nn_params[(self.hidden_layer_1_size*(self.input_layer_size + 1) + self.hidden_layer_2_size*(self.hidden_layer_1_size + 1)):],
                            (self.num_labels, self.hidden_layer_2_size+1) )
        # print('Formato Theta 3', Theta3.shape)
        # print('#####################################')
        
        # Qtde de amostras
        m = X.shape[0]
            
        # As variaveis a seguir precisam ser retornadas corretamente
        J = 0;
        Theta1_grad = np.zeros(Theta1.shape)
        Theta2_grad = np.zeros(Theta2.shape)
        Theta3_grad = np.zeros(Theta3.shape)
        
        if np.min(y) == 0:
            Y_real = np.eye(np.max(y.astype('int') + 1))[y.astype('int')]
        else:
            Y_real = np.eye(np.max(y))[y - 1]

        # Adiciona a coluna do Bias
        bias = np.ones((X.shape[0], 1))
        a1 = np.hstack((bias, X))
        
        # Passa pela primeira camada intermediaria
        z2 = np.dot(a1, Theta1.T)
        a2 = self.sigmoid(z2)
        a2 = np.hstack((bias, a2))
        
        # Passa pela segunda camada intermediaria
        z3 = np.dot(a2, Theta2.T)
        a3 = self.sigmoid(z3)
        a3 = np.hstack((bias, a3))

        # Passa pela terceira camada intermediaria
        z4 = np.dot(a3, Theta3.T)
        a4 = self.sigmoid(z4)

        # Normaliza Theta1
        Theta1_norm = np.sum(np.sum(Theta1[:,1:]**2, axis = 1))
        # Normaliza Theta2
        Theta2_norm = np.sum(np.sum(Theta2[:,1:]**2, axis = 1))
        # Normaliza Theta3
        Theta3_norm = np.sum(np.sum(Theta3[:,1:]**2, axis = 1))
        
        # Calcula o Custo
        J = (1/m) * np.sum(np.sum(- np.multiply(Y_real, np.log(a4 + eps)) - np.multiply((1 - Y_real), np.log(1 - a4 + eps)), axis = 1)) + ( vLambda / (2 * m) ) * (Theta1_norm + Theta2_norm + Theta3_norm)
        
        # Calcula o gradiente para a camada de saída
        delta4 = (a4 - Y_real)
        Theta3_grad = (1 / m) * (Theta3_grad + np.dot(delta4.T, a3))
        Theta3_grad[:, 1:] = Theta3_grad[:, 1:] + ( vLambda / m ) * Theta3[:,1:]
        
        # Calcula o gradiente para a segunda camada intermediaria
        delta3 = np.multiply(np.dot(delta4, Theta3[:, 1:]), self.sigmoid_gradient(z3))
        Theta2_grad = (1 / m) * (Theta2_grad + np.dot(delta3.T, a2))
        Theta2_grad[:, 1:] = Theta2_grad[:, 1:] + ( vLambda / m ) * Theta2[:,1:]

        # Calcula o gradiente para a segunda camada intermediaria
        delta2 = np.multiply(np.dot(delta3, Theta2[:, 1:]), self.sigmoid_gradient(z2))
        Theta1_grad = (1 / m) * (Theta1_grad + np.dot(delta2.T, a1))
        Theta1_grad[:, 1:] = Theta1_grad[:, 1:] + ( vLambda / m ) * Theta1[:,1:]
        ##########################################################################

        # Junta os gradientes
        grad = np.concatenate([np.ravel(Theta1_grad), np.ravel(Theta2_grad), np.ravel(Theta3_grad)])
        
        #duration = timer() - start
        #print('\n[INFO] Tempo de processamento do Calculo do Custo com backpropagation: {}'.format(duration))
        
        return J, grad
    
    def Train(self, X, Y):
        print('[ARQUITETURA] layer_h1 {} / layer_h2 {}'.format(self.hidden_layer_1_size, self.hidden_layer_2_size))
        print('\n[INFO] Inicializando os pesos da rede\n')
        self.input_layer_size  = X.shape[1]
        self.initial_weights()
        print('[INFO] Pesos inicializados com sucesso!')


        print('\n[INFO] Ajustando os pesos da rede neural')
        
        result = scipy.optimize.minimize(fun = self.funcaoCusto_backp_reg, x0 = self.initial_rna_params,
                                            args = (X, Y, self.lambda_value), method = 'TNC', jac = True, options = {'maxiter' : self.nmax_i})
        print(result)
        # Recebe os valores de Theta treinados
        self.nn_trained_params = result.x
        self.Theta1_trained = np.reshape( self.nn_trained_params[0:self.hidden_layer_1_size*(self.input_layer_size + 1)], (self.hidden_layer_1_size, self.input_layer_size + 1) )
        self.Theta2_trained = np.reshape( self.nn_trained_params[self.hidden_layer_1_size*(self.input_layer_size + 1):(self.hidden_layer_1_size*(self.input_layer_size + 1) + self.hidden_layer_2_size*(self.hidden_layer_1_size + 1))], 
                            (self.hidden_layer_2_size, self.hidden_layer_1_size+1) )
        self.Theta3_trained = np.reshape( self.nn_trained_params[(self.hidden_layer_1_size*(self.input_layer_size + 1) + self.hidden_layer_2_size*(self.hidden_layer_1_size + 1)):],
                            (self.num_labels, self.hidden_layer_2_size+1) )

        print('\n[INFO] Pesos da rede neural ajustados')

    def Predict(self, X):
        print('\n[INFO] Classificando novos dados de entradas')
        m = X.shape[0] # Numero de amostras

        # Cria array de predições
        p = np.zeros(m)

        # Passa pelo primeiro Layer Intermediario
        a1 = np.hstack( [np.ones([m,1]),X] )
        h1 = self.sigmoid( np.dot(a1,self.Theta1_trained.T) )
        # Passa pelo segundo Layer Intermediário
        a2 = np.hstack( [np.ones([m,1]),h1] ) 
        h2 = self.sigmoid( np.dot(a2,self.Theta2_trained.T) )
        # Passa pelo Layer de saida
        a3 = np.hstack( [np.ones([m,1]),h2] ) 
        h3 = self.sigmoid( np.dot(a3,self.Theta3_trained.T) )
        # Calcula a maior predição
        p = np.argmax(h3,axis=1)

        return p

    def SaveModel(self, path):
        # Salva CSV que descreve arquitetura da rede
        desc_column = ['Syze_hidden_layer_1', 'Syze_hidden_layer_2', 'Num_classes', 'Lambda_value', 'Max_iterations']
        desc_array = np.reshape(np.array([self.hidden_layer_1_size, self.hidden_layer_2_size, self.num_labels,
                                self.lambda_value, self.nmax_i]), (1, 5))
        df_desc = pd.DataFrame(desc_array, index = None, columns = desc_column)
        df_desc.to_csv(path + r'\Description.csv')
        
        # Salva CSV com os thetas
        df_theta1 = pd.DataFrame(self.Theta1_trained)
        df_theta1.to_csv(path + 'Trained_theta1.csv')
        df_theta2 = pd.DataFrame(self.Theta2_trained)
        df_theta2.to_csv(path + 'Trained_theta2.csv')
        df_theta3 = pd.DataFrame(self.Theta3_trained)
        df_theta3.to_csv(path + 'Trained_theta3.csv')