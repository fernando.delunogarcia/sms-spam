import numpy as np
import CrossValidation as cv
import DNN

class DNN_Models():
    def __init__(self):
        self.hidden_layer_1_size = [32, 64, 128, 256, 512]
        self.hidden_layer_2_size = [32, 64, 128, 256, 512]
        self.lambda_value = [0.01, 0.1, 1, 3, 10]

    def GetModel(self, i):
        if (i < 0) or (i >= (len(self.hidden_layer_1_size) * len(self.hidden_layer_2_size) * len(self.lambda_value)) ):
            return None

        h1_index = int(i/5)%5
        h2_index = int(i/25)%5
        l_index = i%5 

        return DNN.MultilayerPerceptron(self.hidden_layer_1_size[h1_index], self.hidden_layer_2_size[h2_index], 2,
                                         self.lambda_value[l_index], 1000)

    def Count(self):
        return len(self.hidden_layer_1_size) * len(self.hidden_layer_2_size) * len(self.lambda_value)

def GetMultilayerPerceptron(K,model,token):
    bow = 'BIN'
    search = cv.CrossValidation('Multilayer Perceptron', K, model, token, bow, DNN_Models())
    return search