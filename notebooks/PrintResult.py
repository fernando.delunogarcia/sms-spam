import FoldUtils as fd
import numpy as np
import os
import pandas as pd


K = 5
model = 'Normal'
token = 'PF'
bow = 'BIN'

dtypes = [
    '',
    'rdundersampling2',
    'rdundersampling3',
    'rdoversampling2',
    'adasyn2',
    'adasyn3',
    'smote2',
    'smote3',
    'combined2',
    'combined3'
]

dtypes_lbl = [
    'A',
    'B1',
    'B2',
    'C',
    'D1',
    'D2',
    'E1',
    'E2',
    'F1',
    'F2'
]

def PrintDatasets():
    rows = []
    for i in range(len(dtypes)):
        base = dtypes[i]
        folds = fd.FoldReference(K)
        folds.LoadFoldsReference(model,token,bow,base=base)
        df = folds.LoadChunckAndProcess(False, K)

        Y_train = df.iloc[:, -1].values
        #print(base)
        #print("\n\tQtd. de dados de cada classe (treinamento)")
        cTrain, counts_cTrain = np.unique(np.sort(Y_train), return_counts=True)

        row = dtypes_lbl[i] + " : " + dtypes[i]
        row_d = ''
        row_f = ''
        for i in range( len(cTrain) ):
            row_d += ' & %d'%counts_cTrain[i]
            row_f +=' & %1.2f'%((counts_cTrain[i]/len(Y_train))*100)
            #print('\t\tClasse %s: %d (%1.2f%%)' %( cTrain[i],counts_cTrain[i],(counts_cTrain[i]/len(Y_train))*100 ) )
        row += row_d + row_f + '\\\\'
        rows.append(row)

    for row in rows:
        print(row)


def PrintFinalResults():
    path = os.path.abspath('.')
    path = path.split('sms-spam')[0] + 'sms-spam' + os.sep + 'notebooks' + os.sep + 'Outputs'
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if '.csv' in file:
                files.append(os.path.join(r, file))

    db = []
    for f in files:
        df = pd.read_csv(f, sep=',')
        db.append(df)

    columns = db[0].columns
    df_rank = pd.DataFrame(columns = columns)
    df_rank2 = pd.DataFrame(columns = columns)
    
    for df in db:
        #print(df[df["Sampling"].isna()])
        df_rank = df_rank.append(df[df["Sampling"].isna()],ignore_index = True)
        df_rank2 = df_rank2.append(df.loc[0],ignore_index = True)
    
    df_rank = df_rank.sort_values(by=['MCC'],ascending=False)
    
    for i in range(df_rank.shape[0]):
        print("%s & %1.2f & %1.2f & %1.2f & %1.3f \\\\"%(df_rank["Classifier"].iloc[i], df_rank["SC"].iloc[i],df_rank["BH"].iloc[i], df_rank["ACC"].iloc[i], df_rank["MCC"].iloc[i]))
    
    df_rank2 = df_rank2.sort_values(by=['MCC'],ascending=False)

    print('\n\n')
    for i in range(df_rank2.shape[0]):
        print("%s & %1.2f & %1.2f & %1.2f & %1.3f & %s \\\\"%(df_rank2["Classifier"].iloc[i], df_rank2["SC"].iloc[i],df_rank2["BH"].iloc[i], df_rank2["ACC"].iloc[i], df_rank2["MCC"].iloc[i], df_rank2["Sampling"].iloc[i]))


    for i in range(1,len(dtypes)):
        df_rank = pd.DataFrame(columns = columns)
    
        for df in db:
            df_rank = df_rank.append(df[df["Sampling"] == dtypes[i]],ignore_index = True)

        df_rank = df_rank.sort_values(by=['MCC'],ascending=False)
        #print(df_rank)


PrintFinalResults()
#PrintDatasets()
