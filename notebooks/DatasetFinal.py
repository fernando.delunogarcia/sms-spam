import os
import numpy as np
import pandas as pd

import TokenUtils as tkutils
import LanguageModel as mdlutils
import RawDataset as rawdt
import ScoreMethod as smutils

"""
Converte todas as mensagens em tokens separados por vírgula
"""
def PreProcessing(df,tk):
    for i in range(df.shape[0]):
        print("msg" + str(i))
        
        txtr = df["SMS"][i]
        substr = tk.TextTokenization(txtr)

        txt = ''.join(str(e) + ',' for e in substr)
        #if(txt == ''):
        #    print("errr >>> " + str(i))
        df["SMS"][i] = txt     

    tk.WriteTokens()

    return df

def LoadDataset(tokenizer, mdtype, dbpath,dbname):
    lanmodel = mdlutils.getLanguageModel(mdtype)

    dbfullpath = dbpath + dbname + ".txt"
    dbfullpath2 = dbpath + 'FINAL' + ".txt"

    if os.path.exists(dbfullpath) is False:
        print("O arquivo " + dbfullpath + " não existe!")
        return None

    df_dataset = pd.read_csv(dbfullpath, sep='\t', header=None, names = ["Classe", "SMS"])
    df_dataset = PreProcessing(df_dataset,tk)

    try:
        df_dataset.Classe.replace(['ham', 'spam'], [0, 1], inplace=True)
        df_dataset = df_dataset[df_dataset.SMS != '']
        df_dataset.to_csv(dbfullpath2, sep='\t', header=None,index=False)
        del df_dataset
        return rawdt.RawDataset(tokenizer, lanmodel, dbfullpath2, 'FINAL')
        
    except:
        pass

    return None

"""
Grava um arquivo CSV de acordo com a matriz de valores infromada
Caso necessário a matriz é reduzida conforme os atributos indicados.
"""
def ExportBagOfWords(db_path, db_name, db_model,data,freatures):
    DtName = db_path + db_name + '_' + db_model + '.csv'
    df = pd.DataFrame(data=data, columns = freatures)

    df.to_csv(DtName, index=False)
    del df


"""
Deve ser indicado um tokenizador e um modelo de linguagem.
"""
tktype = 'PF' 
mdtype = 'Normal'
tk = tkutils.getTokenizer(tktype)


mypath = os.path.abspath('.')
dbpath = mypath.split('sms-spam')[0] + 'sms-spam' + os.sep + 'dataset' + os.sep
bowpath = mypath.split('sms-spam')[0] + 'sms-spam' + os.sep + 'bow' + os.sep
dbfile = "SMSSpamCollection"



RemoveStopWords = False
RemovePunctuation = True
ConvertNumbers = True

#Opções de base de dados
CreateBinaryBOW = True
CreateOcurrencesBOW = True
CreateInverseTermFrequencyBOW = True

#Configuração das funções de transformação
SmoothFTIDF = True
LogTFIDF = False



tk.LoadTokens(dbpath + '/dictionary.txt') #para simplificar já deixamos o dicionário gerado


#ScoreMethod para marcar a presença de um token no documento
Binary = smutils.BinaryScore()
Ocurrences = smutils.CountScore()
Frequency = smutils.DocFrequencyScore()
      

dbs_train = LoadDataset(tk, mdtype, dbpath, dbfile)

print('Processando dataset de treinamento final')

#criar o vocabulário no dataset de treino
vocabulary,feature_vector = dbs_train.Load(None)
    
print(dbs_train.GetDatasetName())

Binary_Bow = dbs_train.CharacterizeDocuments(Binary)
dfBagOfWords = pd.DataFrame(data=Binary_Bow, columns = feature_vector)

    
if CreateOcurrencesBOW is True:
    print('Processando BOW de ocorrências/discreto')
    #dataset de treinamento
    Ocurrences_Bow = dbs_train.CharacterizeDocuments(Ocurrences)
    ExportBagOfWords(bowpath, dbs_train.GetDatasetName(),'CNT',Ocurrences_Bow,feature_vector)
    del Ocurrences_Bow


if CreateInverseTermFrequencyBOW is True:
    print('Processando BOW TF-IDF')
    TF_IDF = smutils.InverseTermFrequency(dbs_train.total, Binary_Bow[:,:-1],SmoothFTIDF,LogTFIDF, None)
    TFIDF_Bow = dbs_train.CharacterizeDocuments(TF_IDF)
    ExportBagOfWords(bowpath, dbs_train.GetDatasetName(),'TFIDF',TFIDF_Bow,feature_vector)
    
    #salva o IDF do fold
    DtName = bowpath + dbs_train.GetDatasetName() + '_' + 'FeaturesIDF.csv'
    Header = pd.DataFrame(data=TF_IDF.IDF.reshape(1,-1), columns = feature_vector[:-1])
    Header.to_csv(DtName, index=False)
    
    del TFIDF_Bow,TF_IDF

if CreateBinaryBOW is True:
    print('Processando BOW binário')
    ExportBagOfWords(bowpath, dbs_train.GetDatasetName(),'BIN',Binary_Bow,feature_vector)

del dfBagOfWords
