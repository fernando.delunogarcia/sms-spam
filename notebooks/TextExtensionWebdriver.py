import os
import time
import re
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC

class WebDriverUtils:
    txtElem = None
    linkElem = None
    browser = None
    BrowserStarted = False
    last_input = ""
    last_output = ""
    control = ""
    marks = ['<', '>', '%', '*']

    @staticmethod
    def __BrowserInit():
        #abre o navegador e seleciona a opção text-normalization
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        WebDriverUtils.browser = webdriver.Chrome(chrome_options=options)
        #WebDriverUtils.browser = webdriver.Chrome()
        WebDriverUtils.browser.get('http://lasid.sor.ufscar.br/expansion/static/index.html')
        #WebDriverUtils.browser.set_page_load_timeout(10)
        WebDriverUtils.txtElem = WebDriverUtils.browser.find_element_by_id('data_input')
        WebDriverUtils.linkElem = WebDriverUtils.browser.find_element_by_id('submit')
        boxElem = WebDriverUtils.browser.find_element_by_id('lingo')
        boxElem.click()
        WebDriverUtils.BrowserStarted = True
        WebDriverUtils.last_input = ""
        WebDriverUtils.last_output = ""
        WebDriverUtils.control = 0
    @staticmethod
    def FindToken(tk):
             
        try:
            #abre a conexão com o servidor
            if(WebDriverUtils.BrowserStarted is False):
                print('iniciando...')
                WebDriverUtils.__BrowserInit()

            my_input = WebDriverUtils.marks[WebDriverUtils.control] + ' ' + tk


            #indica a nova mensagem que será convertida
            WebDriverUtils.txtElem.clear()
            WebDriverUtils.txtElem.send_keys(my_input)
            WebDriverUtils.linkElem.click()

            #aguarda a atualização da página
            error = False
            caught = False
            while((caught is False)and(error is False)):
                outputElem = WebDriverUtils.browser.find_element_by_id('downloadResult')
                if(outputElem is not None):
                    tmp = outputElem.text
                    if(tmp != ""):
                        if("Our slave hamsters are" not in tmp):
                            if(tmp != WebDriverUtils.last_output):
                                if(tmp[0] == WebDriverUtils.marks[WebDriverUtils.control]):
                                    current = tmp
                                    caught = True
                        else:
                            print('servidor ocupado')
                            error = True
                
            if error is True:
                print('finalizado por erro de serviço -> prox msg deve ser iniciando...')
                WebDriverUtils.BrowserStarted = False
                return None
            else:             
                WebDriverUtils.last_input = my_input
                
                current = re.split('\. |\, ',current)[0] #pega somente o primeiro resultado
                
                WebDriverUtils.last_output = current.replace(WebDriverUtils.marks[WebDriverUtils.control] + ' ','')
        
                WebDriverUtils.control+=1
                if(WebDriverUtils.control >= len(WebDriverUtils.marks)):
                    WebDriverUtils.control=0
                

                return WebDriverUtils.last_output

        except Exception as e: 
            print(e)
            WebDriverUtils.BrowserStarted = False
            return None