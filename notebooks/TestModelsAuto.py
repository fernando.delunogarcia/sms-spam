import TestModels as tm

K = 5
model = 'Normal'
token = 'PF'

#models = [0,1,2,3,4,5,6]
#models = [0,1,2,3,4,5,6,7,8]
#models = [3,4,5,6]
#models = [4]
#models = [7]
models = [0]
#models = [1,2]

#models = [1,2,3,4]

for i in models:
    #o penultimo argumento determina se o dataset será amostrado por várias técnicas
    tm.SelectModelAndRun(K,model,token,True, specific_model=i)
    #tm.SelectModelAndRun(K,model,token,False, specific_model=i)

