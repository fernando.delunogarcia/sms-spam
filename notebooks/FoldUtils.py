import numpy as np
import pandas as pd
import os
import Sampling as sampling

class FoldReference():
    def __init__(self, kfolds):
        self.kfolds = kfolds

    def GetNumberOfFolds(self):
        return self.kfolds

    """
    Armazena a referência para cada dataset
    """
    def LoadFoldsReference(self,model, token, bow, base=''):
        
        #Constrói o caminho relativo até os datasets
        path = os.path.abspath('.')

        if base is '':
            datapath_train = path.split('sms-spam')[0] + 'sms-spam' + os.sep + 'bow' + os.sep
        else:
            datapath_train = path.split('sms-spam')[0] + 'sms-spam' + os.sep + 'bow' + os.sep + base + os.sep

        datapath_valid = path.split('sms-spam')[0] + 'sms-spam' + os.sep + 'bow' + os.sep

        paths = {'TRAIN':datapath_train,
                 'VALID':datapath_valid
        }

        self.folds = {}

        print('Loading References...%d folds'%self.kfolds)
        for i in range(self.kfolds):
            dfs = []
            for j in ['TRAIN', 'VALID']:
                file_path = 'SMS_CROSS_FOLD_{}_{}_{}_TOK_{}_{}'.format(i, j, model, token, bow)
                file_path = paths[j] + file_path
                dfs.append(file_path)
            self.folds['FOLD_' + str(i)] = dfs
        
        #a mesma estrutura é usada para armazenar o dataset completo para validação final
        dfs = []
        for j in ['TRAIN', 'VALID']:
            file_path = 'SMS_{}_{}_TOK_{}_{}'.format(j, model, token, bow)
            file_path = paths[j] + file_path
            dfs.append(file_path)
        self.folds['FOLD_' + str(self.kfolds)] = dfs
        #print(self.folds)

    """
    Carregar o arquivo csv de treino ou validação usando pandas
    """
    def LoadChunckAndProcess(self,is_test_fold,fold):
        fold_key = 'FOLD_' + str(fold)

        if fold_key in self.folds:
            if is_test_fold == False:
                file_path = self.folds[fold_key][0]
            else:
                file_path = self.folds[fold_key][1]
            print(file_path)
            return pd.read_csv(file_path+'.csv', sep=',')

        print('Fold informado não existe: ', fold_key)
        return None

    """
    Retorna o endereço completo do arquivo csv
    """
    def GetFoldPath(self,is_test_fold,fold):
        fold_key = 'FOLD_' + str(fold)

        if fold_key in self.folds:
            if is_test_fold == False:
                file_path = self.folds[fold_key][0]
            else:
                file_path = self.folds[fold_key][1]

            return file_path
        return ""

    """
    Cria um csv conforme o pandas indicado
    """
    def SaveFoldAt(self,is_test_fold,fold,fdir,df):
        fold_key = 'FOLD_' + str(fold)

        if fold_key in self.folds:
            if is_test_fold == False:
                file_path = self.folds[fold_key][0]
            else:
                file_path = self.folds[fold_key][1]

            file_path = file_path.replace('bow', 'bow/'+fdir)
            print(file_path+'.csv')
            df.to_csv(file_path+'.csv', index=False)

        

class FoldSampling():
    def __init__(self, kfolds,model,token,bow):
        self.folds = FoldReference(kfolds)
        self.folds.LoadFoldsReference(model,token,bow)
        self.DTisBoolean = False
        self.DTisContinuous = False
        self.DTisDiscrete = False

        if bow is 'BIN':
            self.DTisBoolean = True
        else:
            if bow is 'CNT':
                self.DTisDiscrete = True
            else:
                self.DTisContinuous = True

    def __Run(self,SamplingMethod):
        K = self.folds.GetNumberOfFolds()
        strategy = SamplingMethod.GetName()
        #print('Carregando base de dados para avaliação do melhor modelo após cada gridsearch/fold')
        print(strategy)

        #dados para treino final do fold
        dftrain = self.folds.LoadChunckAndProcess(False, K)
        Xtrain = dftrain.iloc[:, :-1].values
        Ytrain = dftrain.iloc[:, -1].values
        Xtrain,Ytrain =  SamplingMethod.Sampling(Xtrain,Ytrain)
        self.ExportNewDataset(K,strategy,dftrain,Xtrain,Ytrain)

        for i in range(K):
            #dados para treinamento do modelo
            dff = self.folds.LoadChunckAndProcess(False, i)
            Xt = dff.iloc[:, :-1].values
            Yt = dff.iloc[:, -1].values
            Xt,Yt =  SamplingMethod.Sampling(Xt,Yt)    
            self.ExportNewDataset(i,strategy,dff,Xt,Yt)

    def ExportNewDataset(self, i, sdir, df, X, Y):
        print(df.shape)
        new_df = pd.DataFrame(data=np.column_stack((X, Y)),columns = df.columns)
        
        self.folds.SaveFoldAt(False,i,sdir,new_df)
                
        print(new_df.shape)

    def RunRandomUndersampling(self, df=None):
        for i in range(2,4):
            ratio = i/10.0
            sm = sampling.RandomUnderSampling(ratio=ratio)
            self.__Run(sm)


    def RunRandomOversampling(self, df=None):
        for i in range(2,3):
            ratio = i/10.0
            sm = sampling.RandomOverSampling(ratio=ratio)
            self.__Run(sm)

    def RunSyntheticOverSampling(self, df=None):
        for i in range(2,4):
            ratio = i/10.0
            
            if self.DTisContinuous is True:
                sm = sampling.SyntheticOverSampling(ratio=ratio)
            else:
                sm = sampling.SyntheticOverSampling(ratio=ratio,discrete=True)

            self.__Run(sm)

    def RunAdaptativeSyntheticOverSampling(self, df=None):
        for i in range(2,4):
            ratio = i/10.0
            
            if self.DTisContinuous is True:
                sm = sampling.AdaptativeSyntheticOverSampling(ratio=ratio)    
            else:
                sm = sampling.AdaptativeSyntheticOverSampling(ratio=ratio,discrete=True)
            
            self.__Run(sm)

    def RunCombinedSampling(self, df=None):
        for i in range(2,4):
            ratio = i/10.0

            if self.DTisContinuous is True:
                sm = sampling.SMOTEENN_Sampling(ratio=ratio)
            else:
                sm = sampling.SMOTEENN_Sampling(ratio=ratio,discrete=True)

            self.__Run(sm)


"""
K = 5
model = 'Normal'
token = 'PF'
bow = 'BIN'
fs = FoldSampling(K,model,token,bow)
fs.RunRandomUndersampling()
fs.RunRandomOversampling()
fs.RunSyntheticOverSampling()
fs.RunAdaptativeSyntheticOverSampling()
fs.RunCombinedSampling()
fs.RunUndersampling()

bow = 'CNT'
fs = FoldSampling(K,model,token,bow)
fs.RunRandomUndersampling()
fs.RunRandomOversampling()
fs.RunSyntheticOverSampling()
fs.RunAdaptativeSyntheticOverSampling()
fs.RunCombinedSampling()
fs.RunUndersampling()

bow = 'TFIDF'
fs = FoldSampling(K,model,token,bow)
fs.RunRandomUndersampling()
fs.RunRandomOversampling()
fs.RunSyntheticOverSampling()
fs.RunAdaptativeSyntheticOverSampling()
fs.RunCombinedSampling()
fs.RunUndersampling()
"""
"""
path = os.path.abspath('.')
relative_path = path.split('sources')[0] + 'sources' + os.sep + 'bow' + os.sep
K = 5
model = 'OSB-3'
token = 'PF'
bow = 'TFIDF'

def MyFunc(df):
    print(df.shape)

fr = FoldReference(10,K)
fr.LoadFoldsReference(relative_path, model, token, bow)
fr.LoadChunckAndProcess(False,0,MyFunc)
"""