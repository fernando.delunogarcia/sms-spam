import AbstractClassifier as ac
import numpy as np
import svmutil
from svmutil import svm_read_problem
from svmutil import svm_problem
from svmutil import svm_parameter
from svmutil import svm_train
from svmutil import svm_predict
from svmutil import svm_save_model

class LIBSVM_SupportVectorMachine(ac.AbstractClassier):
    def __init__(self, cost):
        self.Cost = cost
        

    def GetModelName(self):
        return 'SVM ' + 'C=' + str(self.Cost)

    def Train(self, X, Y):
        kernel = 0
        self.model = svm_train(Y, X, '-q -c %f -t %d' %(self.Cost, kernel))

    def Predict(self, X):
        Ytmp = np.zeros(X.shape[0])
        pred,metrics,values = svm_predict(Ytmp,X,self.model)
        return np.reshape(pred,X.shape[0])

    def SaveModel(self, path):
        pass