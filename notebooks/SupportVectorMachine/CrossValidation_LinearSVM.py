from pathlib import Path
import sys
#d = Path().resolve().parent
#sys.path.append('../')

import os
import numpy as np
import CrossValidation as cv

class LinearSVM_Models():
    def __init__(self):
        
        self.cost = 2**np.linspace(0, 10, num=11) #melhor condição obtida C=2


    def GetModel(self,i):
        if (i < 0) or (i >= len(self.cost)):
            return None

        import LIBSVM_SupportVectorMachine as lib_svm
        return lib_svm.LIBSVM_SupportVectorMachine(self.cost[i])

    def Count(self):
        return len(self.cost)


def GetLinearSVM(K,model,token):
    bow = 'TFIDF'
    search = cv.CrossValidation('SVM Linear', K, model, token, bow, LinearSVM_Models())
    return search