import os
import numpy as np
import pandas as pd

import TokenUtils as tkutils
import LanguageModel as mdlutils
import HoldOut as hd
import Kfold as kf
import RawDataset as rawdt
import ScoreMethod as smutils
import DatasetStatistics as dsutils
import TextExtensionWebdriver as te
import FoldUtils as futils

RANDOM_SEED_KFOLD = 10
RANDOM_SEED_HOLDOUT = 10

"""
Executa procedimento K-fold estratificado retornando o conjunto de dados
sorteado e os índices utilizados em cada etapa
"""
def KFolds(nFolds,Y):
    # semente usada na randomizacao dos dados.
    randomSeed = RANDOM_SEED_KFOLD 

    # gera os indices aleatorios que irao definir a ordem dos dados
    idx_perm = np.random.RandomState(randomSeed).permutation(range(len(Y)))

    # ordena os dados de acordo com os indices gerados aleatoriamente
    # separa os dados em k folds
    folds = kf.stratified_kfolds(Y[idx_perm], nFolds) 

    return folds

"""
Executa procedimento holdout estratificado retornando o conjunto de dados
sorteado e os índices utilizados na divisão do dataset
"""
def HoldOut(pTrain,Y):
    # semente usada na randomizacao dos dados.
    randomSeed = RANDOM_SEED_HOLDOUT 

    # gera os indices aleatorios que irao definir a ordem dos dados
    idx_perm = np.random.RandomState(randomSeed).permutation(range(len(Y)))

    # ordena os dados de acordo com os indices gerados aleatoriamente
    # obtem os indices dos dados da particao de treinamento e da particao de teste
    fold = hd.stratified_holdOut(Y[idx_perm], pTrain)

    return fold

"""
Função utilizada para criar um arquivo txt dados os valores de classe e SMS
"""
def CreatePandas(X,Y,path,name):
    df = pd.DataFrame({
        'Classe': pd.Series(Y),
        'SMS': pd.Series(X),
    })

    df.to_csv(path+name+'.txt', sep='\t', encoding='utf-8', index=False, header=None)

"""
Função utilizada para criar as bases de teste e treino da etapa K-fold
Apresenta as estatísticas do fold.
"""
def ExportDataset(X_train, X_test, Y_train, Y_test, samples, path,name):
    #grava os arquivos txt
    CreatePandas(X_train, Y_train, path,name+'_TRAIN')
    CreatePandas(X_test, Y_test, path,name+'_VALID')

    print('\tQtd. dados de treinamento: %d (%1.2f%%)' %(X_train.shape[0], (X_train.shape[0]/samples)*100) )
    print('\tQtd. de dados de teste: %d (%1.2f%%)' %(X_test.shape[0], (X_test.shape[0]/samples)*100) )

    # imprime a porcentagem de dados de treinamento de cada classe
    print("\n\tQtd. de dados de cada classe (treinamento)")
    cTrain, counts_cTrain = np.unique(np.sort(Y_train), return_counts=True)
    for i in range( len(cTrain) ):
        print('\t\tClasse %s: %d (%1.2f%%)' %( cTrain[i],counts_cTrain[i],(counts_cTrain[i]/len(Y_train))*100 ) )

    # imprime a porcetagem de dados de teste de cada classe
    print("\n\tQtd. de dados de cada classe (teste)")
    cTest, counts_cTest = np.unique(np.sort(Y_test), return_counts=True)
    for i in range( len(cTrain) ):
        print('\t\tClasse %s: %d (%1.2f%%)' %( cTest[i],counts_cTest[i],(counts_cTest[i]/len(Y_test))*100 ) )

"""
Converte todas as mensagens em tokens separados por vírgula
"""
def PreProcessing(df,tk):
    for i in range(df.shape[0]):
        #print("msg" + str(i))
        
        txtr = df["SMS"][i]
        substr = tk.TextTokenization(txtr)

        txt = ''.join(str(e) + ',' for e in substr)
        #if(txt == ''):
        #    print("errr >>> " + str(i))
        df["SMS"][i] = txt     

    tk.WriteTokens()

    return df

       
"""
Função genérica para criar uma base de dados no model holdout ou k-fold
O parâmetro blocks define esse formato, sendo 0 para holdout ou maior que zero
indicando o valor K de folds que serão criados.
"""
def CreateRawDataset(file,path,outputname,pholdout,blocks, tk):
    if os.path.exists(file) is False:
        print('O arquivo indicado não existe.')
        return False

    # importa da base de dados indicada
    if blocks == 0:
        df_dataset = pd.read_csv(file, sep='\t', header=None, names = ["Classe", "SMS"])
        if(tk is not None):
            df = PreProcessing(df_dataset,tk)

            #remove amostras duplicadas ou nulas
            df.to_csv(path+'SMS_PREPROC.txt', sep='\t', header=None,index=False)
            df.drop_duplicates(subset ="SMS", keep = 'first', inplace = True)
            df = df[df.SMS != '']
            
            X = df.iloc[:, 1].values 
            Y = df.iloc[:, 0].values
        else:
            X = df_dataset.iloc[:, 1].values 
            Y = df_dataset.iloc[:, 0].values
        
        #substitui a string que representa a classe pelos valores 0 e 1
        Y[Y=="ham"] = 0
        Y[Y=="spam"] = 1
        folds = HoldOut(pholdout,Y)
        DB_NAME = outputname
        
    else:
        df_dataset = pd.read_csv(file, sep='\t', header=None, names = ["Classe", "SMS"])
        # Pega os valores das n-1 primeiras colunas e guarda em uma matrix X
        # Pega os valores da última coluna e guarda em um vetor Y
        X = df_dataset.iloc[:, 1].values 
        Y = df_dataset.iloc[:, 0].values
        folds = KFolds(blocks, Y)

        DB_NAME = outputname + '_FOLD_'


    #procedimento final para exportar os elementos ppara um arquivo de teste
    k = 0
    for train_index, test_index in folds:
        if(train_index in test_index):
            print('error')

        if blocks > 0:
            DB_NAME_TMP = DB_NAME + str(k)
            print('\n-----------\n%d-fold: \n-----------\n' % (k) )
        else:
            DB_NAME_TMP = DB_NAME
            print('\n-----------\nHoldout \n-----------\n')

        # se train_index ou test_index forem vazios, interrompe o laco de repeticao
        if len(train_index)==0 or len(test_index)==0: 
            print('\tErro: o vetor com os indices de treinamento ou o vetor com os indices de teste esta vazio')      
            break

        #faz a seleção dos dados conforme os índices retornados nas funções de divisão
        X_train, X_test = X[train_index], X[test_index]
        Y_train, Y_test = Y[train_index], Y[test_index]

        ExportDataset(X_train, X_test, Y_train, Y_test, X.shape[0], path, DB_NAME_TMP)
        k+=1

    if(k == folds.shape[0]):
        return True
    else:
        return False

"""
Essa função faz a divisão inicial do dataset em treinamento e 
teste de acordo com o percentual de divisão informado.
A base original é dividida e gravada em dois arquivos separados.
"""
def SplitRawDataset(pholdout, path, db, dbname, tk):
    db_full_path = path + db

    if (pholdout >= 20) or (pholdout <= 90):
        print('Importando base de dados de SMS.')
        CreateRawDataset(db_full_path, path, dbname, pholdout/100, 0, tk)
        print('Procedimento de divisão finalizado:' , pholdout, 'treinamento.')
        return True
    else:
        print('Percentual de treino inválido.')

    return False


def LoadDataset(tokenizer, mdtype, dbpath, dbname):
    lanmodel = mdlutils.getLanguageModel(mdtype)

    dbfullpath = dbpath + dbname + ".txt"

    if os.path.exists(dbfullpath) is False:
        print("O arquivo " + dbfullpath + " não existe!")
        return None

    return rawdt.RawDataset(tokenizer, lanmodel, dbfullpath, dbname)

"""
Função para ler um conjunto de dados em texto e transformar em um modelo de BagOfWords
Os nomes devem estar no padrão: NOME_ID_MODO.txt

Por exemplo:
SMS_CROSS_FOLD_0_TRAIN.txt
"""
def LoadDatasets(tokenizer, mdtype, K, dbpath, dbn, dbm):
    dbs = np.zeros(K, dtype='object')

    for i in range(K):
        dbname = dbn + str(i) + dbm
        
        dbs[i] = LoadDataset(tokenizer, mdtype, dbpath, dbname)
        print(dbs[i].GetDatasetName())

    return dbs

"""
Grava um arquivo CSV de acordo com a matriz de valores infromada
Caso necessário a matriz é reduzida conforme os atributos indicados.
"""
def ExportBagOfWords(db_path, db_name, db_model,data, IDX,freatures):
    DtName = db_path + db_name + '_' + db_model + '.csv'
    df = pd.DataFrame(data=data, columns = freatures)

    if IDX is not None:
        df = df.iloc[:,IDX]
    df.to_csv(DtName, index=False)
    del df


def CreateNewDatasets(K,model,token,bow):
    print('Realizando procedimentos de amostragem')
    fs = futils.FoldSampling(K,model,token,bow)
    fs.RunRandomUndersampling()
    fs.RunRandomOversampling()
    fs.RunSyntheticOverSampling()
    fs.RunAdaptativeSyntheticOverSampling()
    fs.RunCombinedSampling()

ptrain = 70 #divisão do dataset
SplitDataset = True

"""
Deve ser indicado um tokenizador e um modelo de linguagem.
"""
tktype = 'PF' 
mdtype = 'Normal'
tk = tkutils.getTokenizer(tktype)


mypath = os.path.abspath('.')
dbpath = mypath.split('sms-spam')[0] + 'sms-spam' + os.sep + 'dataset' + os.sep
bowpath = mypath.split('sms-spam')[0] + 'sms-spam' + os.sep + 'bow' + os.sep
dbfile = "SMSSpamCollection.txt" #base original
SMS_TRAIN = "SMS_TRAIN" #base de treinamento no modo k-fold
SMS_TEST = "SMS_VALID" #base que será utilizado após a etapa de treinamento
SMS_CROSS = 'SMS_CROSS'



RemoveStopWords = False
RemovePunctuation = True
ConvertNumbers = True

#Opções de base de dados
CreateBinaryBOW = True
CreateOcurrencesBOW = True
CreateInverseTermFrequencyBOW = True

#Configuração das funções de transformação
SmoothFTIDF = True
LogTFIDF = False

RemoveFeatures = False
InformationGainThreshold = 0.002


"""
A primeira parte do procedimento de construção do dataset consiste na divisão das amostras
em um parcela para treinamento e outra para teste de avaliação final dos modelos.

A função SplitRawDataset faz tal divisão utilizando na base de mensagens original.
O resultado da resultado são dois arquivos de texto contendo percentual equivalente de classes.
"""
if SplitDataset is True:
    tk.LoadTokens(dbpath + '/dictionary.txt') #para simplificar já deixamos o dicionário gerado
    status = SplitRawDataset(ptrain, dbpath, dbfile,'SMS',tk)
else:
    tk.LoadTokens(dbpath + '/dictionary.txt')
    status = True


if status is True:
    K = 5

    #ScoreMethod para marcar a presença de um token no documento
    Binary = smutils.BinaryScore()
    Ocurrences = smutils.CountScore()
    Frequency = smutils.DocFrequencyScore()
      
    ################################################################
    ################################################################
    #Dataset final de treino e teste após validação cruzada

    dbs_train = LoadDataset(tk, mdtype, dbpath, SMS_TRAIN)
    dbs_test = LoadDataset(tk, mdtype, dbpath, SMS_TEST)

    print('Processando dataset de treinamento e teste')

    #criar o vocabulário no dataset de treino
    vocabulary,feature_vector = dbs_train.Load(None)
    
    Binary_Bow = dbs_train.CharacterizeDocuments(Binary)
    dfBagOfWords = pd.DataFrame(data=Binary_Bow, columns = feature_vector)

    #verifica se foi marcada a opção para reduzir dimensionalidade baseado no ganho de informação do atríbuto
    if RemoveFeatures is True:
        statistics = dsutils.DatasetStatistics(dfBagOfWords)
        IG = statistics.EvaluateInformationGain()
        IG_idx = statistics.GetInformationGainGreaterThan(InformationGainThreshold)
        IDX = np.append(IG_idx[0], len(vocabulary))
    else:
        IDX = list(range(Binary_Bow.shape[1]))
    
    #a base de teste é carregada com vocabulário obtido na base de treinamento
    new_features = np.take(feature_vector,IDX)
    dbs_test.Load(new_features)
    Binary_BowTest = dbs_test.CharacterizeDocuments(Binary)

    if CreateOcurrencesBOW is True:
        print('Processando BOW de ocorrências/discreto')
        #dataset de treinamento
        Ocurrences_Bow = dbs_train.CharacterizeDocuments(Ocurrences)
        ExportBagOfWords(bowpath, dbs_train.GetDatasetName(),'CNT',Ocurrences_Bow,IDX,feature_vector)
        
        #dataset de teste
        Ocurrences_Bow = dbs_test.CharacterizeDocuments(Ocurrences)
        ExportBagOfWords(bowpath, dbs_test.GetDatasetName(),'CNT',Ocurrences_Bow,None,new_features)
        del Ocurrences_Bow

    if CreateInverseTermFrequencyBOW is True:
        print('Processando BOW TF-IDF')
        #dataset de treinamento
        #Neste processo o array de IDF será criado
        #Posteriormente será aplicado no dataset de teste
        TF_IDF = smutils.InverseTermFrequency(dbs_train.total, Binary_Bow[:,:-1],SmoothFTIDF,LogTFIDF, None)
        TFIDF_Bow = dbs_train.CharacterizeDocuments(TF_IDF)
        ExportBagOfWords(bowpath, dbs_train.GetDatasetName(),'TFIDF',TFIDF_Bow,IDX,feature_vector)
        
        #teste
        TF_IDF_teste = smutils.InverseTermFrequency(dbs_train.total, None,SmoothFTIDF,LogTFIDF, TF_IDF.IDF[IDX[:-1]])
        TFIDF_Bow = dbs_test.CharacterizeDocuments(TF_IDF_teste)
        ExportBagOfWords(bowpath, dbs_test.GetDatasetName(),'TFIDF',TFIDF_Bow,None,new_features)
        del TFIDF_Bow,TF_IDF

    if CreateBinaryBOW is True:
        print('Processando BOW binário')
        ExportBagOfWords(bowpath, dbs_train.GetDatasetName(),'BIN',Binary_Bow,IDX,feature_vector)
        
        ExportBagOfWords(bowpath, dbs_test.GetDatasetName(),'BIN',Binary_BowTest,None,new_features)
        del Binary_BowTest,Binary_Bow
    del dfBagOfWords
    
    
    
    """
    A base de treinamento será dividida em K-folds de treinamento e validação
    Esse procedimento cria Kx2 arquivos de texto para cada etapa de treinamento.

    A função CreateRawDataset faz essa divisão conforme a base de dados indicada
    e a quantidade de divisões.
    """
    
    status = CreateRawDataset(dbpath+SMS_TRAIN+".txt", dbpath, SMS_CROSS, 0, K, tk)
    
    
    
    if status is True: 
        dbs_train = LoadDatasets(tk, mdtype, K, dbpath, 'SMS_CROSS_FOLD_', "_TRAIN")
        dbs_test = LoadDatasets(tk, mdtype, K, dbpath, 'SMS_CROSS_FOLD_', "_VALID")
        
        #Carrega o dataset e cria vocabulário
        for i in range(K):
            print('Processando dataset de treinamento e validação do fold %d'%(i+1))

            #criar o vocabulário no dataset de treino
            vocabulary,feature_vector = dbs_train[i].Load(None)
            
            Binary_Bow = dbs_train[i].CharacterizeDocuments(Binary)
            dfBagOfWords = pd.DataFrame(data=Binary_Bow, columns = feature_vector)

            #verifica se foi marcada a opção para reduzir dimensionalidade baseado no ganho de informação do atríbuto
            if RemoveFeatures is True:
                statistics = dsutils.DatasetStatistics(dfBagOfWords)
                IG = statistics.EvaluateInformationGain()
                IG_idx = statistics.GetInformationGainGreaterThan(InformationGainThreshold)
                IDX = np.append(IG_idx[0], len(vocabulary))
            else:
                IDX = list(range(Binary_Bow.shape[1]))
            
            #a base de teste é carregada com vocabulário obtido na base de treinamento
            new_features = np.take(feature_vector,IDX)
            dbs_test[i].Load(new_features)
            Binary_BowTest = dbs_test[i].CharacterizeDocuments(Binary)

            if CreateOcurrencesBOW is True:
                print('Processando BOW de ocorrências/discreto')
                #dataset de treinamento
                Ocurrences_Bow = dbs_train[i].CharacterizeDocuments(Ocurrences)
                ExportBagOfWords(bowpath, dbs_train[i].GetDatasetName(),'CNT',Ocurrences_Bow,IDX,feature_vector)
                
                #dataset de teste
                Ocurrences_Bow = dbs_test[i].CharacterizeDocuments(Ocurrences)
                ExportBagOfWords(bowpath, dbs_test[i].GetDatasetName(),'CNT',Ocurrences_Bow,None,new_features)
                del Ocurrences_Bow

            if CreateInverseTermFrequencyBOW is True:
                print('Processando BOW TF-IDF')
                #dataset de treinamento
                #Neste processo o array de IDF será criado
                #Posteriormente será aplicado no dataset de teste
                TF_IDF = smutils.InverseTermFrequency(dbs_train[i].total, Binary_Bow[:,:-1],SmoothFTIDF,LogTFIDF, None)
                TFIDF_Bow = dbs_train[i].CharacterizeDocuments(TF_IDF)
                ExportBagOfWords(bowpath, dbs_train[i].GetDatasetName(),'TFIDF',TFIDF_Bow,IDX,feature_vector)

                #teste
                TF_IDF_teste = smutils.InverseTermFrequency(dbs_train[i].total, None,SmoothFTIDF,LogTFIDF, TF_IDF.IDF[IDX[:-1]])
                TFIDF_Bow = dbs_test[i].CharacterizeDocuments(TF_IDF_teste)
                ExportBagOfWords(bowpath, dbs_test[i].GetDatasetName(),'TFIDF',TFIDF_Bow,None,new_features)
                del TFIDF_Bow,TF_IDF

            if CreateBinaryBOW is True:
                print('Processando BOW binário')
                ExportBagOfWords(bowpath, dbs_train[i].GetDatasetName(),'BIN',Binary_Bow,IDX,feature_vector)

                ExportBagOfWords(bowpath, dbs_test[i].GetDatasetName(),'BIN',Binary_BowTest,None,new_features)
                del Binary_BowTest,Binary_Bow
            del dfBagOfWords

    if CreateBinaryBOW is True:
        CreateNewDatasets(K,mdtype,tktype,'BIN')
    if CreateOcurrencesBOW is True:
        CreateNewDatasets(K,mdtype,tktype,'CNT')
    if CreateInverseTermFrequencyBOW is True:
        CreateNewDatasets(K,mdtype,tktype,'TFIDF')       

"""
#salva o IDF do fold
DtName = bowpath + dbs_train[i].GetDatasetName() + '_' + 'FeaturesIDF.csv'
Header = pd.DataFrame(data=TF_IDF_teste.IDF.reshape(1,-1), columns = new_features[:-1])
Header.to_csv(DtName, index=False)
"""