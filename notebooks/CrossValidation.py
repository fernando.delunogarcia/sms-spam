import numpy as np
import pandas as pd
import datetime as dt
import FoldUtils as fr
import GridSearch as gs
import CrossValidationProcedure as cv
import Sampling as sampling

class CrossValidation():
    def __init__(self, Name, K, model, token, bow, models, metric='MCC'):
        self.Grid = gs.GridSearch(models)

        self.TestName = Name + ' + ' + model
        self.Model = model
        self.Token = token
        self.BOW = bow

        #Carrega as referências para o fold de treinamento
        self.Folds = fr.FoldReference(K)
        
        self.Sampling = None
        self.Results = None
        self.Metric = metric


    def Run(self):

        if self.Grid is not None:

            if self.Grid.Count() > 0:

                if self.Sampling is not None:
                    strategy = self.Sampling.GetName()
                else:
                    strategy = ''

                self.Folds.LoadFoldsReference(self.Model, self.Token, self.BOW,base=strategy)

                #cross-validation e gridsearch avaliado pelo MCC
                cross = cv.CrossValidationProcedure(self.Folds, self.Grid)
                report = cross.Train(self.Metric)

                best_model = report['MODEL']
                model = self.Grid.GetModel(best_model)

                
                #procedimento para gravar parâmetros do melhor modelo obtido
                now = dt.datetime.now()
                output = '../Outputs/' + self.TestName + '_' + strategy+ '_' + now.strftime("%Y-%m-%d_%H-%M")
                print('Melhor modelo foi gravado em: ', output)
                model.SaveModel(output)

                #construção do relatório de desempenho
                df = self.__CreateFinalReport(model, report, strategy)

                if self.Results is None:
                    self.Results = df
                else:
                    self.Results = self.Results.append(df,ignore_index = True)
            else:
                print('Models = None')

            self.Sampling = None

    def Annotate(self, base):
        if self.Grid.Count() > 0:
            now = dt.datetime.now()
            output = base + self.TestName + '_' + '_' + now.strftime("%Y-%m-%d_%H-%M")
            rname = output+'.csv'
            header = ['Classifier','MCC', 'ACC', 'SC', 'BH','F-measure','Sampling']
            df = self.Results.sort_values(by=[self.Metric], ascending=False)
            df.to_csv(rname, encoding='utf-8', header=header,index=False)
            print(df)
            print('Relatório gravado em: ', rname)
            
    def RunRandomUndersampling(self, df=None):
        for i in range(2,4):
            ratio = i/10.0
            self.Sampling = sampling.RandomUnderSampling(ratio=ratio)
            self.Run()

    def RunRandomOversampling(self, df=None):
        for i in range(2,3):
            ratio = i/10.0
            self.Sampling = sampling.RandomOverSampling(ratio=ratio)
            self.Run()

    def RunSyntheticOverSampling(self, df=None):
        for i in range(2,4):
            ratio = i/10.0
            self.Sampling = sampling.SyntheticOverSampling(ratio=ratio)
            self.Run()

    def RunAdaptativeSyntheticOverSampling(self, df=None):
        for i in range(2,4):
            ratio = i/10.0
            self.Sampling = sampling.AdaptativeSyntheticOverSampling(ratio=ratio)    
            self.Run()

    def RunCombinedSampling(self, df=None):
        for i in range(2,4):
            ratio = i/10.0
            self.Sampling = sampling.SMOTEENN_Sampling(ratio=ratio)
            self.Run()

    def __CreateFinalReport(self,model,report, strategy='Normal'):

        df = pd.DataFrame({
            'Classifier': pd.Series(model.GetModelName()),
            'MCC': pd.Series(report['MCC']),
            'ACC': pd.Series(report['ACC']),
            'SC': pd.Series(report['SC']),
            'BH': pd.Series(report['BH']),
            'F-measure': pd.Series(report['FM']),
            'Sampling': pd.Series(strategy)
        })
        return df


