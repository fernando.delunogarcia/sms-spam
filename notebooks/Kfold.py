import os
import numpy as np
import pandas as pd
from math import ceil

def stratified_kfolds(target, k):
    # Inicializa Vetor onde cada i-item contém dois arrays (1 - Index de treino da i-Fold / 2 - Index de teste da i-Fold)
    folds_final = np.zeros( k,dtype='object')

    # Lista que armazena a quantidade de ocorrências de cada classe p/ o percentual
    qt_classes = []
    index_classes = []
    classes = np.unique(target)
    print(classes)
    
    for i in classes:
        index_classes.append(np.where(target == i)[0])
        qt_classes.append(int(len(target[target==i])))
    pass_class = np.array([ceil(qt / k) for qt in qt_classes])
    
    for k_value in range(k):
        # Separa a final do conjunto de dados como validação
        if k_value == (k - 1):
            train_index = np.sort(np.concatenate(([index_classes[i][:(k - 1)*pass_class[i]] for i in range(len(index_classes))])))
            test_index = np.sort(np.concatenate(([index_classes[i][(k - 1)*pass_class[i]:] for i in range(len(index_classes))]))) 
            folds_final[k_value] = np.array( [train_index,test_index] ) 
        # Separa o final do conjunto de dados como validação
        elif k_value == 0:
            train_index = np.sort(np.concatenate(([index_classes[i][pass_class[i]:] for i in range(len(index_classes))])))
            test_index = np.sort(np.concatenate(([index_classes[i][:pass_class[i]] for i in range(len(index_classes))]))) 
            folds_final[k_value] = np.array( [train_index,test_index] ) 
        else:
            train_index_bf = np.sort(np.concatenate(([index_classes[i][:k_value * pass_class[i]] for i in range(len(index_classes))]))) 
            train_index_af = np.sort(np.concatenate(([index_classes[i][(k_value * pass_class[i]) + pass_class[i]:] for i in range(len(index_classes))])))
            train_index = np.append(train_index_bf, train_index_af)
            test_index = np.sort(np.concatenate(([index_classes[i][k_value * pass_class[i]:(k_value * pass_class[i]) + pass_class[i]] for i in range(len(index_classes))]))) 
            folds_final[k_value] = np.array( [train_index,test_index] ) 
    
    return folds_final