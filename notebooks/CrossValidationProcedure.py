import numpy as np
import PerformanceMeasures as pm

class CrossValidationProcedure():
    def __init__(self, folds, models):
        self.folds = folds  #referência para carregar os folds de treinamento
        self.models = models #bjeto para criação dinâmica de modelos para treinamento

    def Train(self,performance_metric='MCC'):
        report = None
        K = self.folds.GetNumberOfFolds()

        print('Carregando base de dados para avaliação do melhor modelo após cada gridsearch/fold')

        #dados para treino final do fold
        self.dftrain = self.folds.LoadChunckAndProcess(False, K)
        self.Xtrain = self.dftrain.iloc[:, :-1].values
        self.Ytrain = self.dftrain.iloc[:, -1].values
        
        #dados para teste do modelo
        self.dftest = self.folds.LoadChunckAndProcess(True, K)
        self.Xtest = self.dftest.iloc[:, :-1].values
        self.Ytest = self.dftest.iloc[:, -1].values
        
        #executa o treinamento em todos os folds
        report = self.__TrainFolds(performance_metric)

        return report
        
    def __TrainFolds(self, performance_metric):
        
        #variáveis de execução do treinamento
        BestScore = 0
        BestModel = 0

        K = self.folds.GetNumberOfFolds()
        FoldMeasures = np.zeros(K, dtype='object')
        
        M = self.models.Count()
        BestModelsCounter = np.zeros(M)

        for i in range(K):
            #dados para treinamento do modelo
            dft = self.folds.LoadChunckAndProcess(False, i)
            Xt = dft.iloc[:, :-1].values
            Yt = dft.iloc[:, -1].values

            #dados para teste do modelo
            dfv = self.folds.LoadChunckAndProcess(True, i)
            Xv = dfv.iloc[:, :-1].values
            Yv = dfv.iloc[:, -1].values
        
            #executa o fold K para cada modelo M
            #isto resulta no gridsearch para o fold K
            print('#####################################################################')
            print('FOLD ', str(i))
            print('#####################################################################')

            #GridSearch
            self.models.Train(Xt, Yt)
            reports = self.models.Predict(Xv,Yv)

            #Procura pelo modelo com melhor desempenho conforme a métrica especificada
            for j in range(len(reports)):
                if(j == 0):
                    BestScore = reports[j][performance_metric]
                else:
                    if(BestScore < reports[j][performance_metric]):
                        BestScore = reports[j][performance_metric]
                        BestModel = j

            del dft, dfv, Xt, Yt, Xv, Yv

            #contabiliza o uso deste modelo
            BestModelsCounter[BestModel] += 1
            
            #treina o modelo com a base completa e faz avaliação na base de teste
            model = self.models.GetModel(BestModel)

            if model is not None:
                print('>>> Best model: ', model.GetModelName())
                print('Training...')
                model.Train(self.Xtrain, self.Ytrain)
                print('Testing...')
                Y_pred = model.Predict(self.Xtest)

                #preenche o report de um fold com os resultados obtidos pelo melhor modelo avaliado
                FoldMeasures[i] = pm.PerformanceMeasures()
                FoldMeasures[i].EvaluatePerformanceMetrics(self.Ytest, Y_pred)
                print(FoldMeasures[i].GetPerformanceReport())
                
            print('#####################################################################')

        #calcula os valores médios do modelo M
        avg_reports = self.__GetAvgMeasures(FoldMeasures,BestModelsCounter)
        return avg_reports

    def __GetAvgMeasures(self, reports, BestModelsCounter):
        
        N = len(reports)
        acuracia = np.zeros(N)
        fmedida = np.zeros(N)
        mcc = np.zeros(N)
        sc = np.zeros(N)
        bh = np.zeros(N)

        #copia as métricas de todos os folds
        for i in range(N):
            report = reports[i].GetPerformanceReport()
            acuracia[i] = report['ACC']
            fmedida[i] = report['FM']
            mcc[i] = report['MCC']
            sc[i] = report['SC']
            bh[i] = report['BH']

        #define o modelo que venceu mais entre todos os folds
        best = np.where(BestModelsCounter == np.amax(BestModelsCounter))
            
        cv_resultado = {'ACC' : np.mean(acuracia), 
                        'MCC': np.mean(mcc), 
                        'SC': np.mean(sc), 
                        'BH': np.mean(bh), 
                        'FM': np.mean(fmedida),
                        'MODEL' : best[0][0]
                        }

        return cv_resultado
 

