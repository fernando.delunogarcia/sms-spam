from abc import ABC, abstractmethod
import numpy as np

class AbstractSampling(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def Sampling(self,X,Y):
        pass

    @abstractmethod
    def GetName(self):
        pass

    def Print(self,Y):
        idx = np.where(Y == 0)[0]
        p = idx.shape[0]/Y.shape[0]
        print('Ham: ', p, ' Spam: ', 1-p, len(idx), Y.shape[0]- len(idx))


class RandomUnderSampling(AbstractSampling):
    def __init__(self, seed=0,ratio=0.2):
        self.Seed = seed
        self.Ratio = ratio

    def Sampling(self,X,Y):
        from imblearn.under_sampling import RandomUnderSampler
        print('Undersampling... %1.2f'%self.Ratio)

        super().Print(Y)
        rus = RandomUnderSampler(random_state=self.Seed,sampling_strategy=self.Ratio)
        X,Y = rus.fit_resample(X, Y)
        super().Print(Y)
        
        return X,Y

    def GetName(self):
        return 'rdundersampling%d'%(10*self.Ratio)

class RandomOverSampling(AbstractSampling):
    def __init__(self, seed=0,ratio=0.2):
        self.Seed = seed
        self.Ratio = ratio

    def Sampling(self,X,Y):
        from imblearn.over_sampling import RandomOverSampler
        print('Oversampling... %1.2f'%self.Ratio)
        super().Print(Y)
        ros = RandomOverSampler(random_state=self.Seed,sampling_strategy=self.Ratio)
        X,Y = ros.fit_resample(X, Y)
        super().Print(Y)
        
        return X,Y

    def GetName(self):
        return 'rdoversampling%d'%(10*self.Ratio)


class SyntheticOverSampling(AbstractSampling):
    def __init__(self, seed=0,ratio=0.2, discrete=False):
        self.Seed = seed
        self.Ratio = ratio
        self.Round = discrete

    def Sampling(self,X,Y):
        from imblearn.over_sampling import SVMSMOTE
        from imblearn.over_sampling import BorderlineSMOTE
        print('Oversampling... %1.2f'%self.Ratio)
        super().Print(Y)
        #ros = BorderlineSMOTE(random_state=self.Seed,sampling_strategy=self.Ratio)
        ros = SVMSMOTE(random_state=self.Seed,sampling_strategy=self.Ratio)
        X,Y = ros.fit_resample(X, Y)

        if self.Round is True:
            X = np.round(X)

        super().Print(Y)
        
        return X,Y

    def GetName(self):
        return 'smote%d'%(10*self.Ratio)

class AdaptativeSyntheticOverSampling(AbstractSampling):
    def __init__(self, seed=0,ratio=0.2,discrete=False):
        self.Seed = seed
        self.Ratio = ratio
        self.Round = discrete

    def Sampling(self,X,Y):
        from imblearn.over_sampling import ADASYN
        print('Oversampling... %1.2f'%self.Ratio)
        super().Print(Y)
        ros = ADASYN(random_state=self.Seed,sampling_strategy=self.Ratio)
        X,Y = ros.fit_resample(X, Y)

        if self.Round is True:
            X = np.round(X)
        super().Print(Y)
        
        return X,Y
    
    def GetName(self):
        return 'adasyn%d'%(10*self.Ratio)

class SMOTEENN_Sampling(AbstractSampling):
    def __init__(self, seed=0,ratio=0.2,discrete=False):
        self.Seed = seed
        self.Ratio = ratio
        self.Round = discrete

    def Sampling(self,X,Y):
        from imblearn.combine import SMOTEENN
        print('SMOTE Edited NN... %1.2f'%self.Ratio)
        super().Print(Y)
        sme = SMOTEENN(random_state=self.Seed,sampling_strategy=self.Ratio)
        X,Y = sme.fit_resample(X, Y)
        
        if self.Round is True:
            X = np.round(X)
        
        super().Print(Y)
        
        return X,Y

    def GetName(self):
        return 'combined%d'%(10*self.Ratio)
