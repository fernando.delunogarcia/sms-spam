import sys
sys.path.insert(0, './SupportVectorMachine')
sys.path.insert(0, './NaiveBayes')
sys.path.insert(0, './KNearestNeighbors')
sys.path.insert(0, './LogRegression')
sys.path.insert(0, './MultilayerPerceptron')

from NaiveBayes import CrossValidation_BooleanNaiveBayes as blnnb
from NaiveBayes import CrossValidation_BasicNaiveBayes as bnb
from NaiveBayes import CrossValidation_MultinomialBooleanNaiveBayes as mnb
from NaiveBayes import CrossValidation_MultinomialTermFrequencyNaiveBayes as mtfnb
from NaiveBayes import CrossValidation_MultivariateBernoulliNaiveBayes as mvbnb
from NaiveBayes import CrossValidation_MultivariateGaussNaiveBayes as mvgnb
from SupportVectorMachine import CrossValidation_LinearSVM as lsvm
from KNearestNeighbors import CrossValidation_KNearestNeighbors as knn
from LogRegression import CrossValidation_LogisticRegression as lreg
from MultilayerPerceptron import CrossValidation_DNN as mlp

#cada modelo deve conter uma função que retorna o objeto de CV
GetModels = {
    'SVM': lsvm.GetLinearSVM,
    'MN Booelan NB Laplace': mnb.GetMNBooleanNB_LAPLACE,
    'MN TermFreq NB Laplace': mtfnb.GetMNfrequencyNB_LAPLACE,
    'MN Booelan NB Lidstone': mnb.GetMNBooleanNB_LIDSTONE,
    'MN TermFreq NB Lidstone': mtfnb.GetMNfrequencyNB_LIDSTONE,
    'MV Bernoulli NB': mvbnb.GetMVBernoulliNB,
    'MV Gauss NB': mvgnb.GetMVGaussNB,
    'Boolean NB': blnnb.GetBooleanNB,
    'Basic NB': bnb.GetBasicNB,
    'KNN': knn.GetKNN,
    'Logictic Regression': lreg.GetLogisticRegression,
    'MLP' : mlp.GetMultilayerPerceptron
}

def PrintModels():
    i = 0
    for key,func in GetModels.items():
        print(i, key)
        i += 1

def SelectModelAndRun(K, model, token, run_all,specific_model=-1):
    cv_models = []

    for key,func in GetModels.items():
        cv_models.append(func)

    id_model = 0
    while id_model >= 0:
        
        if(specific_model >= 0):
            id_model = specific_model
        else:
            PrintModels()
            id_model = int(input("Selecione um modelo ou -1 para sair: "))

        if id_model >= 0:
            cv_model = cv_models[id_model](K,model,token)

            cv_model.Run()
            #cv_model.Annotate('./Outputs/')
            if run_all is True:
                cv_model.RunRandomUndersampling()
                #cv_model.Annotate('./Outputs/')
                cv_model.RunRandomOversampling()
                #cv_model.Annotate('./Outputs/')
                cv_model.RunSyntheticOverSampling()
                #cv_model.Annotate('./Outputs/')
                cv_model.RunAdaptativeSyntheticOverSampling()
                #cv_model.Annotate('./Outputs/')
                cv_model.RunCombinedSampling()
                #cv_model.Annotate('./Outputs/')
                
            cv_model.Annotate('./Outputs/')

        if(specific_model >= 0):
            id_model = -1 #exit

def SelectModel(i):
    K = 5
    model = 'Normal'
    token = 'PF'

    #o ultimo argumento determina se o dataset será amostrado por várias técnicas
    SelectModelAndRun(K,model,token,False,i)