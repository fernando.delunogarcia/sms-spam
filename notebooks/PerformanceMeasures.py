import numpy as np


class PerformanceMeasures():
    def __init__(self):
        #considerando somente duas classes
        self.classes = [0, 1]
        self.nClasses = 2
        # inicializa as medidas que deverao ser calculadas
        self.vp= 0
        self.vn= 0
        self.fp= 0
        self.fn= 0
        self.resultados = {}
        self.Cs = 0
        self.Cl = 0

    def EvaluatePerformanceMetrics(self,Y_test, Y_pred):
        self.__UpdateConfusionMatrix(Y_test, Y_pred)
        self.__EvaluatePerformance()
        
        return self.resultados

    def GetPerformanceReport(self):
        return self.resultados

    def __UpdateConfusionMatrix(self,Y_test, Y_pred):
        #somente binária
        
        #seleciono os índices em que a classe é spam
        pj = np.where(Y_test == 1)
        if(pj[0].shape[0]>0):
            self.Cs = pj[0].shape[0]
            self.vp = np.equal(Y_test[pj], Y_pred[pj]).nonzero()[0].shape[0]
            self.fn = np.not_equal(Y_test[pj], Y_pred[pj]).nonzero()[0].shape[0]
        else:
            self.Cs = 0
            self.vp = 0
            self.fn =0

        #seleciono os índices em que a classe é ham
        pj = np.where(Y_test == 0)
        if(pj[0].shape[0]>0):
            self.Cl = pj[0].shape[0]
            self.vn = np.equal(Y_test[pj], Y_pred[pj]).nonzero()[0].shape[0]
            self.fp = np.not_equal(Y_test[pj], Y_pred[pj]).nonzero()[0].shape[0]
        else:
            self.Cl = 0
            self.vn = 0
            self.fp = 0


    def __EvaluatePerformance(self):
        
        #Recall
        den = (self.vp+self.fn)
        if den > 0:
            revocacao = self.vp/den
        else:
            revocacao = 0

        #Precision
        den = (self.vp+self.fp)
        if den > 0:
            precisao = self.vp/den
        else:
            precisao = 0

        #F-measure
        den = (precisao+revocacao)

        if(den > 0):
            fmedida = 2*((precisao*revocacao)/den)
        else: 
            fmedida = 0
        
        #Accuracy
        acuracia = (self.vp + self.vn)/(self.vp+self.vn+self.fp+self.fn)
        
        #Mathews Correlation Coefficient
        alpha = 1
        mcc_den = np.sqrt((self.vp + alpha*self.fp)*(self.vp+self.fn)*(self.vn + alpha*self.fp)*(self.vn+self.fn))
        if(mcc_den > 0):
            mcc = ((self.vp*self.vn)-(alpha*self.fp*self.fn))/mcc_den
        else:
            mcc = 0
        
        
        #spam caught
        if(self.Cs > 0):
            sc = self.vp/self.Cs
        else:
            sc = 0

        #blocked ham
        if(self.Cl > 0):
            bh = self.fp/self.Cl
        else:
            bh = 0
        
        # guarda os resultados em uma estrutura tipo dicionario
        self.resultados = {
                    'MCC': mcc, 
                    'ACC': acuracia*100, 
                    'SC': sc*100, 
                    'BH': bh*100,
                    'FM':fmedida,
                    }