import numpy as np
import CrossValidation as cv
import BasicNaiveBayes as bnb

class BasicNB_Models():
    def __init__(self):
        pass
        
    def GetModel(self,i):
        if (i < 0) or (i >= 1):
            return None

        return bnb.BasicNaiveBayes()

    def Count(self):
        return 1


def GetBasicNB(K,model,token):
    bow = 'BIN'
    search = cv.CrossValidation('Basic NB', K, model, token, bow, BasicNB_Models())
    return search