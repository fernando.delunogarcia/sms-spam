import numpy as np
import CrossValidation as cv
import MultivariateGaussNaiveBayes as gnb

class MVGaussNB_Models():
    def __init__(self):
        pass

    def GetModel(self,i):
        if (i < 0) or (i >= 1):
            return None

        return gnb.GaussNaiveBayes()

    def Count(self):
        return 1


def GetMVGaussNB(K,model,token):
    bow = 'TFIDF'
    search = cv.CrossValidation('MV Gauss NB', K, model, token, bow, MVGaussNB_Models())
    return search