from pathlib import Path
import sys
d = Path().resolve().parent
sys.path.append('../')

import AbstractClassifier as ac
import numpy as np 
import math

class MultinomialNaiveBayes(ac.AbstractClassier):
    def __init__(self, boolean, alpha):
        self.boolean = boolean
        self.alpha = alpha

    def GetModelName(self):
        if self.boolean is True:
            return 'MN Boolean NB' + ' Alpha = ' + str(self.alpha)
        else:
            return 'MN TermFreq NB' + ' Alpha = ' + str(self.alpha)

    def Train(self, X, Y):
        self.__EvaluateProbabilities(X,Y)

    def Predict(self, X):
        pred = np.zeros( X.shape[0] )

        for i in range(X.shape[0]):
            pred[i] = self.__Predict(X[i,:])

        return pred

    def SaveModel(self, path):
        pass

    def __EvaluateProbabilities(self, X, Y):
        """
        CALCULARPROBABILIDADES Computa a probabilidade de ocorrencia de cada 
        atributo por rotulo possivel. A funcao retorna dois vetores de tamanho n
        (qtde de atributos), um para cada classe.
        
        CALCULARPROBABILIDADES(X, Y) calcula a probabilidade de ocorrencia de cada atributo em cada classe. 
        Cada vetor de saida tem dimensao (n x 1), sendo n a quantidade de atributos por amostra.
        """

        self.cardinality = X.shape[1]

        # Probabilidade das Classes
        self.pSpam = sum(Y==1)/len(Y) 
        self.pHam = sum(Y==0)/len(Y)

        #  inicializa os vetores de probabilidades
        self.pAtrSpam = np.zeros(X.shape[1])
        self.pAtrHam = np.zeros(X.shape[1])

        #somatória de ocorrências por atríbuto da classe spam
        idx_S = np.where(Y == 1)
        self.pAtrSpam = np.sum(X[idx_S[0],:],axis=0)
        #total de ocorrências entre todos os atríbutos relacionados com a classe spam
        count_S = np.sum(self.pAtrSpam)
        #suavização
        self.pAtrSpam = (self.pAtrSpam+self.alpha)/(count_S+self.alpha*self.cardinality)
        
        #somatória de ocorrências por atríbuto da classe ham
        idx_H = np.where(Y == 0)
        self.pAtrHam = np.sum(X[idx_H[0],:],axis=0)
        count_H = np.sum(self.pAtrHam)
        #suavização
        self.pAtrHam = (self.pAtrHam+self.alpha)/(count_H+self.alpha*self.cardinality)


    def __Predict(self,X):
        """
        Classifica se a entrada x pertence a classe 0 ou 1 usando
        as probabilidades extraidas da base de treinamento. Essa funcao 
        estima a predicao de x atraves da maior probabilidade da amostra  
        pertencer a classe 1 ou 0.
        """

        #  inicializa a classe e as probabilidades condicionais
        classe = 0
        probSpam= 0
        probHam = 0
        
        
        probSpam = self.pSpam * np.prod(self.pAtrSpam**X)
        probHam = self.pHam * np.prod(self.pAtrHam**X)

        
        if probSpam > probHam:
            classe = 1

        return classe 