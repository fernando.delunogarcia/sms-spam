import numpy as np
import CrossValidation as cv
import MultinomialNaiveBayes as mnb

class MNbooleanNB_Models():
    def __init__(self,laplace):
        if laplace is True:
            self.alphas = [1]
        else:
            self.alphas = np.linspace(0.1, 0.9, num=9)

    def GetModel(self,i):
        if (i < 0) or (i >= len(self.alphas)):
            return None

        return mnb.MultinomialNaiveBayes(True, self.alphas[i])

    def Count(self):
        return len(self.alphas)


def GetMNBooleanNB_LIDSTONE(K,model,token):
    bow = 'BIN'
    search = cv.CrossValidation('MN Boolean NB Lidstone', K, model, token, bow, MNbooleanNB_Models(False))
    return search

def GetMNBooleanNB_LAPLACE(K,model,token):
    bow = 'BIN'
    search = cv.CrossValidation('MN Boolean NB Laplace', K, model, token, bow, MNbooleanNB_Models(True))
    return search