from pathlib import Path
import sys
d = Path().resolve().parent
sys.path.append('../')

import AbstractClassifier as ac
import numpy as np 

class BernoulliNaiveBayes(ac.AbstractClassier):
    def __init__(self, boolean):
        self.boolean = boolean

    def GetModelName(self):
        if(self.boolean is False):
            return 'MV Bernoulli NB'
        else:
            return 'Boolean NB'

    def Train(self, X, Y):
        self.__EvaluateProbabilities(X,Y)

    def Predict(self, X):
        pred = np.zeros( X.shape[0] )

        for i in range(X.shape[0]):
            pred[i] = self.__Predict(X[i,:])

        return pred

    def SaveModel(self, path):
        pass

    def __EvaluateProbabilities(self, X, Y):
        """
        CALCULARPROBABILIDADES Computa a probabilidade de ocorrencia de cada 
        atributo por rotulo possivel. A funcao retorna dois vetores de tamanho n
        (qtde de atributos), um para cada classe.
        
        CALCULARPROBABILIDADES(X, Y) calcula a probabilidade de ocorrencia de cada atributo em cada classe. 
        Cada vetor de saida tem dimensao (n x 1), sendo n a quantidade de atributos por amostra.
        """

        self.cardinality = X.shape[1]

        # Probabilidade das Classes
        self.pSpam = sum(Y==1)/len(Y) 
        self.pHam = sum(Y==0)/len(Y)

        #  inicializa os vetores de probabilidades
        self.pAtrSpam = np.zeros(X.shape[1])
        self.pAtrHam = np.zeros(X.shape[1])

        #seleciono os índices em que Y é igual a um e determino a quantidade de elementos
        #computo o total de ocorrência para cada coluna. sum(xj_i) considerando que cj = 1
        
        idx_S = np.where(Y == 1)
        count_S = idx_S[0].shape[0]
        
        #calculo a probabilidade considerando que a somatória já representa a qtd. de elementos
        self.pAtrSpam = np.sum(X[idx_S[0],:],axis=0)
        self.pAtrSpam = (self.pAtrSpam+1)/(count_S+2)
        
        #Faço o mesmo para Y==0
        idx_H = np.where(Y == 0)
        count_H = idx_H[0].shape[0]
        
     
        self.pAtrHam = np.sum(X[idx_H[0],:],axis=0)
        self.pAtrHam = (self.pAtrHam+1)/(count_H+2)


    def __Predict(self,X):
        """
        Classifica se a entrada x pertence a classe 0 ou 1 usando
        as probabilidades extraidas da base de treinamento. Essa funcao 
        estima a predicao de x atraves da maior probabilidade da amostra  
        pertencer a classe 1 ou 0.
        """

        #  inicializa a classe e as probabilidades condicionais
        classe = 0
        probSpam= 0
        probHam = 0
        
        #Valores booleanos

        if self.boolean is False:
            #Multivariate Bernouli
            probSpam = self.pAtrSpam*X + (1-self.pAtrSpam)*(1-X)
            probHam = self.pAtrHam*X + (1-self.pAtrHam)*(1-X)
        else:
            #Boolean NB
            nonzero = X > 0
            probSpam = self.pAtrSpam[nonzero]
            probHam = self.pAtrHam[nonzero]

        probSpam = self.pSpam * np.prod(probSpam)
        probHam = self.pHam * np.prod(probHam)
        
        
        if probSpam > probHam:
            classe = 1

        return classe 