import numpy as np
import CrossValidation as cv
import MultivariateBernoulliNaiveBayes as mvbnb

class MVBernoulliNB_Models():
    def __init__(self):
        pass

    def GetModel(self,i):
        if (i < 0) or (i >= 1):
            return None

        #passo o argumento False para operar no Modo MV Bernoulli    
        return mvbnb.BernoulliNaiveBayes(False)

    def Count(self):
        return 1


def GetMVBernoulliNB(K,model,token):
    bow = 'BIN'
    search = cv.CrossValidation('MV Bernoulli NB', K, model, token, bow, MVBernoulliNB_Models())
    return search