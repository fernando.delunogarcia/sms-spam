from pathlib import Path
import sys
d = Path().resolve().parent
sys.path.append('../')

import AbstractClassifier as ac
import numpy as np 

class GaussNaiveBayes(ac.AbstractClassier):
    def __init__(self):
        pass

    def GetModelName(self):
        return 'MV Gauss NB'

    def Train(self, X, Y):
        self.__EvaluateProbabilities(X,Y)

    def Predict(self, X):
        pred = np.zeros( X.shape[0] )

        for i in range(X.shape[0]):
            pred[i] = self.__Predict(X[i,:])

        return pred

    def SaveModel(self, path):
        pass

    def __EvaluateProbabilities(self, X, Y):
        """
        CALCULARPROBABILIDADES Computa a probabilidade de ocorrencia de cada 
        atributo por rotulo possivel. A funcao retorna dois vetores de tamanho n
        (qtde de atributos), um para cada classe.
        
        CALCULARPROBABILIDADES(X, Y) calcula a probabilidade de ocorrencia de cada atributo em cada classe. 
        Cada vetor de saida tem dimensao (n x 1), sendo n a quantidade de atributos por amostra.
        """

        self.cardinality = X.shape[1]

        # Probabilidade das Classes
        self.pSpam = sum(Y==1)/len(Y) 
        self.pHam = sum(Y==0)/len(Y)

        #seleciono os índices em que Y é igual a um e determino a quantidade de elementos
        idx_S = np.where(Y == 1)
        self.spam_mean = np.mean(X[idx_S[0],:],axis=0)
        self.spam_std = np.sqrt(np.var(X[idx_S[0],:],axis=0,ddof=1))
        
        #Faço o mesmo para Y==0
        idx_H = np.where(Y == 0)
        self.ham_mean = np.mean(X[idx_H[0],:],axis=0)
        self.ham_std = np.sqrt(np.var(X[idx_H[0],:],axis=0,ddof=1))

    def __Predict(self,X):
        """
        Classifica se a entrada x pertence a classe 0 ou 1 usando
        as probabilidades extraidas da base de treinamento. Essa funcao 
        estima a predicao de x atraves da maior probabilidade da amostra  
        pertencer a classe 1 ou 0.
        """

        #  inicializa a classe e as probabilidades condicionais
        classe = 0
        probSpam= 0
        probHam = 0

        def probability(x,mean,std):
            exp = np.exp(-0.5*(np.power((x - mean)/std, 2)))
            return exp * (1 / (np.sqrt(2 * np.pi) * std))

        nonzero = self.spam_std > 0
        probSpamX = probability(X[nonzero], self.spam_mean[nonzero], self.spam_std[nonzero])
        nonzero = self.ham_std > 0
        probHamX = probability(X[nonzero], self.ham_mean[nonzero], self.ham_std[nonzero])
        
        probSpam = self.pSpam * np.prod(probSpamX)
        probHam = self.pHam * np.prod(probHamX)
        
        if probSpam > probHam:
            classe = 1

        return classe 