import numpy as np
import CrossValidation as cv
import MultinomialNaiveBayes as mnb

class MNfrequencyNB_Models():
    def __init__(self,laplace):
        if laplace is True:
            self.alphas = [1]
        else:
            self.alphas = np.linspace(0.1, 0.9, num=9)

    def GetModel(self,i):
        if (i < 0) or (i >= len(self.alphas)):
            return None

        return mnb.MultinomialNaiveBayes(False, self.alphas[i])

    def Count(self):
        return len(self.alphas)


def GetMNfrequencyNB_LIDSTONE(K,model,token):
    bow = 'CNT'
    search = cv.CrossValidation('MN TermFreq NB Lidstone', K, model, token, bow, MNfrequencyNB_Models(False))
    return search

def GetMNfrequencyNB_LAPLACE(K,model,token):
    bow = 'CNT'
    search = cv.CrossValidation('MN TermFreq NB Laplace', K, model, token, bow, MNfrequencyNB_Models(True))
    return search




