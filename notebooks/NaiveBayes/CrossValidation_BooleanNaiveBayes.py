import numpy as np
import CrossValidation as cv
import MultivariateBernoulliNaiveBayes as bnb

class MNbooleanNB_Models():
    def __init__(self):
        pass

    def GetModel(self,i):
        if (i < 0) or (i >= 1):
            return None

        #informo o argumento True para que o MV Bernoulli contabilize somente as ocorrência,
        #tornando-se Boolean NB
        return bnb.BernoulliNaiveBayes(True)

    def Count(self):
        return 1


def GetBooleanNB(K,model,token):
    bow = 'BIN'
    search = cv.CrossValidation('Boolean NB', K, model, token, bow, MNbooleanNB_Models())
    return search