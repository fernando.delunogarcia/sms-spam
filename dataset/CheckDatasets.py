import os
import pandas as pd



def CheckDuplicates(train,test):
    for sms_test in test["SMS"]:
        if sms_test in train["SMS"]:
            return True
    return False

def LoadDatasetsAndCheck(train,test):
    df_train = pd.read_csv(train, sep='\t', header=None, names = ["Classe", "SMS"])
    #print(df_train.head())

    df_test = pd.read_csv(test, sep='\t', header=None, names = ["Classe", "SMS"])
    #print(df_test.head())

    print(train)
    print(test)
    duplicates = CheckDuplicates(df_train, df_test)
    if(duplicates is True):
        print('Amostras replicadas em teste e validação')
    else:
        print('Não contém amostras replicadas')

    print(sum(df_train.duplicated(subset="SMS")*1))
    print(sum(df_test.duplicated(subset="SMS")*1))



mypath = os.path.abspath('.')
dbpath = mypath.split('sms-spam')[0] + 'sms-spam' + os.sep + 'dataset' + os.sep

dbtrainpath = dbpath + 'SMS_TRAIN.txt'
dbtestpath = dbpath + 'SMS_VALID.txt'

LoadDatasetsAndCheck(dbtrainpath,dbtestpath)

for i in range(5):
    dbtrainpath = dbpath + 'SMS_CROSS_FOLD_%d_TRAIN.txt'%i
    dbtestpath = dbpath + 'SMS_CROSS_FOLD_%d_VALID.txt'%i

    LoadDatasetsAndCheck(dbtrainpath,dbtestpath)
